{-# LANGUAGE Arrows, NoMonomorphismRestriction #-}

{-|
Module      : XmlCBMC
Description :  Parsers  xml files from the CBMC tool
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental

This module parse the xml result from CBMC /(C bounded model checking tool)/.

Using  the /cbmc/ with the tag /--xml-ui/ produces an  output in xml. This module parse that xml resulting solver status and the trace if violation was obtained.


-}

module XML.XmlCBMC where

--  Module responsible to parseXML
import Text.XML.HXT.Core hiding (trace)

-- | Data containing the relevant information from xml file
data CBMCxml = CBMCxml {  getStatus :: Bool -- ^ Prover Status, if was found a violation.
                         ,getTrace::[(String,String,String)]-- ^ The trace that led to the violation. Each element is tuple with 3 elements.  
                         --,getIndex:: Maybe String -- ^ Assert line violation for 2 version
--
-- * The fist value is the variable name. 
--
-- * The second is the value in the variable at that point. 
--
-- * The last one is the variable type.
--
-- The list is temporal assignments during code execution.

                       }
   deriving (Show,Eq)

-- |
--  Parses the xml to a structure where later will be extract information with environment pre-set .
--  The environment is set to:
--
-- * withValidate no
-- 
-- * withRemoveWS yes
--
parseXML file = readDocument [ withValidate no
                             , withRemoveWS yes  -- throw away formatting WS
                             ] file

-- | Obtain a tag structure. Can be applied recursively.
atTag tag = deep (isElem >>> hasName tag)

-- | Obtain the text in the tag
tagText = (getText <<< deep isText)

-- | converts the text in tag cprover, which identify of violation occurred, into boolean value.
parseStatus [] = error "CBMC run fail during exection. File was accepted. Probably cbmc bug."
parseStatus ["SUCCESS"] = True
parseStatus _ = False

-- | Function responsible in convert all the xml structure in treated data
trace = atTag "cprover" >>> atTag "goto_trace"  >>> listA assign
  
status = atTag "cprover" >>> atTag "cprover-status" >>> tagText
  
--line = atTag "cprover" >>>  atTag "goto_trace" >>> atTag "failure" >>> atTag "line" >>> tagText


-- | Element that obtain the variable assignments from the trace that lead to the violation.
assign = atTag "assignment" >>>
  proc assg -> do
    name <- getAttrValue "display_name" -< assg -- the systax is <function>::<varname> as string
    value <- tagText <<< atTag "value" -< assg
    valType <- tagText <<< atTag "type" -< assg
    returnA -< (name,value,valType)


--xmlcheck

xmlFileTrace f = xmlTrace (parseXML f )

xmlStrTrace f = xmlTrace (readString [] f) 

xmlTrace xml = do 
  s <- runX (xml >>> status)
  if (parseStatus s) then return $ [CBMCxml (parseStatus s) [] {-Nothing-}] 
  else do
    t <- runX (xml >>> trace )
    return $ [CBMCxml (parseStatus s) (concat t) {-(Just(concat l))-} ]





