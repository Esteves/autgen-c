{-|
Module      : G-Genarator
Description : The main file Responsible for IO communication. 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental

-}
module Main where

import System.Environment
import Automation
import Opt

main = do
  opt <- getArgs
  opts opt >>= automation
  
