module Configurations where

prefix = "cc_gen_"
infix_ = "_"

location = "cc_loc_" 

randomInt = "nondet_int"

randomChar = "nondet_char"

random = "nondet_"

atribVars = "cc_atr_"

fileSructRun = "CBMCfile.c"

wtestFile = "C-GEN_Tests.txt"

funcNonDet s =  random ++ s
