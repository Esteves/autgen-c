-----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
-- Um módulo contendo
-- %s/^[^-][^-]/-- /g 
-- %s/-- type \([A-Za-z]\+\) = \([A-Za-z]\+\) Annot/type \1Annot = \2 Annot/g
-----------------------------------------------------------------------------
module Types.AST where
import Language.C.Syntax.AST
import Types.Annotations
import Language.C.Data.Node

-- | Language.C.Syntax.AST with with list of Annotations 
type CTranslUnitAnnot = CTranslationUnit Annot
type CExtDeclAnnot = CExternalDeclaration Annot
type CFunDefAnnot = CFunctionDef Annot
type CDeclAnnot = CDeclaration Annot
type CDeclrAnnot = CDeclarator Annot
type CDerivedDeclrAnnot = CDerivedDeclarator Annot
type CArrSizeAnnot = CArraySize Annot
type CStatAnnot = CStatement Annot
type CAsmStmtAnnot = CAssemblyStatement Annot
type CAsmOperandAnnot = CAssemblyOperand Annot
type CBlockItemAnnot = CCompoundBlockItem Annot
type CDeclSpecAnnot = CDeclarationSpecifier Annot
type CStorageSpecAnnot = CStorageSpecifier Annot
type CTypeSpecAnnot = CTypeSpecifier Annot
type CTypeQualAnnot = CTypeQualifier Annot
type CStructUnionAnnot = CStructureUnion Annot
type CEnumAnnot = CEnumeration Annot
type CInitAnnot = CInitializer Annot
type CInitListAnnot = CInitializerList Annot
type CDesignatorAnnot = CPartDesignator Annot
type CAttrAnnot = CAttribute Annot
type CExprAnnot = CExpression Annot
type CBuiltinAnnot = CBuiltinThing Annot
type CConstAnnot = CConstant Annot
type CStrLitAnnot = CStringLiteral Annot

-- | 
-- 
init :: CTranslUnit -> CTranslUnitAnnot 
init = fmap (\x-> ([],[],[],x) ) 



undefAnnot = ([],[],[],undefNode)

