module Types.Option where


data TargetCoverage = Decision | DecisionVar | DecisionPathSet deriving (Eq,Show)

data Unit = Single | Select | Dependencies deriving (Eq,Show)


data Options = Options
    { optCoverage     :: [TargetCoverage]
    , optMaxBound     :: [Int]
    , optMinBound     :: [Int]
    , targetF         :: String
    , help            :: Bool
    , verbose         :: Bool
    , unitType        :: Unit
    , unitList        :: [String]
    } deriving (Show, Eq)
