module Types.Paths where

import Types.CGF
import Data.Set
import Data.List as L


import System.CPUTime
import Text.Printf

type Path = Set Int
type Paths = [Path]

greaterPath [] = empty
greaterPath ls = L.maximumBy (\x y-> if ( (size x) == (size  y) )
                                  then compare x y 
                                  else compare (size x) (size y)
                                  ) ls


createPath :: [Int] -> Path
createPath = fromList
-- could be fromDistinctAscList --

remove :: (Path -> [Path]->[Path])
remove = L.delete

createPaths = L.map (createPath)

isEmpty p = (L.length p) == 0

rmLocals l ls = L.delete empty . toAscList . fromList $ (L.map (\x-> difference x l) ls) 


