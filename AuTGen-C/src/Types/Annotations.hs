{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, FlexibleContexts #-}
-----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
--Um módulo contendo
-----------------------------------------------------------------------------

module Types.Annotations where
import Language.C.Syntax.AST
import Text.PrettyPrint.HughesPJ
import Language.C.Data.Node
import Language.C.Data.Position (position)
type Annot = Annotation NodeInfo
-- | Will use this type to annotations.
-- Note is @Note ()@ to re-utilize the CCExpression. 
type Annotation a = ( [Note],   -- pre block annotation 
                      [Note],   --  in block annotation {- While,... -}
                      [Note],   -- pos block annotation
                      a )      


pre  :: Annotation a -> [Note]
pre (a,_,_,_) = a
body :: Annotation a -> [Note]
body (_,b,_,_) = b
pos  :: Annotation a -> [Note]
pos (_,_,c,_) = c


preUpdata  :: ( [Note] -> [Note] ) -> Annotation a -> Annotation a
preUpdata f (a,b,c,d) = (f a,b,c,d)


instance CNode Annot where
  nodeInfo (_,_,_,a) = a
  
data Note = IFDef !Int [ Note ] 
                    -- ^ represent the #ifdef ...  #endif
                  | Assume (CExpression Annot)
                    -- ^ represent the assume 
                  | Assert (CExpression Annot)
                     -- ^ represent the assert
                  | Var Int 
                     -- ^
                    deriving ({-Data,Typeable,-} Show {-! ,CNode , Annotated !-})
                    


