module Types.CGF where

data CGF a b  = C b -- Call function
              | R -- Return
              | B -- Break
              | Cont
              | Id a  -- Location
              | Switch [CGF a b]
              | Case [CGF a b]
              | If ([CGF a b],[CGF a b])
              | Cycle ([CGF a b],[CGF a b])
  deriving (Eq,Show)


{--  examples
d = [(Ciclo ([  Id 'b'  , (If ( [(Id 'c'), (Ciclo ([  Id 'e'  , (If ( [(Id 'g')], [(Id 'h'),(If ( [(Id 'i')], [(Id 'j')])) ])) ]  , [Id 'f']) ) ], [(Id 'd')])) ]  , [C 'a']) )]

d4=[(Ciclo ([  Id 'f'  , (If ( [(Id 'g')], [(Id 'h'),(If ( [(Id 'i')], [(Id 'j')])) ])) ]  , [Id 'f']) )]
d1 =  [(Id 'h')]
d2 =  [(If ( [(Id 'h')], [(Id 'd')]))]
d3 = [(Ciclo ([  Id 'x'  , (If ( [(Id 'y')], [(Id 'z')])) ]  , [C 'w']) )]
-}
