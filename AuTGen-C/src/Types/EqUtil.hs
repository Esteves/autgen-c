{-# LANGUAGE TypeSynonymInstances, FlexibleInstances  #-}
module Types.EqUtil where

import Language.C.Syntax


 -- ForThe externalF first elem
instance Eq (CTypeSpecifier a) where -- CTypeSpecifier NodeInfo
  (CVoidType _)         == (CVoidType _)          = True
  (CCharType _)         == (CCharType _)          = True
  (CShortType _)        == (CShortType _)         = True
  (CIntType _)          == (CIntType _)           = True
  (CLongType _)         == (CLongType _)          = True
  (CFloatType _)        == (CFloatType _)         = True
  (CDoubleType _)       == (CDoubleType _)        = True
  (CSignedType _)       == (CSignedType _)        = True
  (CUnsigType _)        == (CUnsigType _)         = True
  (CBoolType _)         == (CBoolType _)          = True
  (CComplexType _)      == (CComplexType _)       = True
  (CSUType _ _)         == (CSUType _ _)      = True
  (CEnumType _ _)       == (CEnumType _ _)     = True
  (CTypeDef a _)        == (CTypeDef a' _)     = a == a'
  (CTypeOfExpr _ _)     == (CTypeOfExpr _ _)   = True -- ??
  (CTypeOfType _ _)     == (CTypeOfType _ _)   = True -- ??
  _ == _ = False


-- CStruct 
--    CStructTag
--    (Maybe Ident)
--    (Maybe [CDecl])    -- member declarations
--    [CAttr]            -- __attribute__s
--    NodeInfo
instance Eq CStructUnion where
  (CStruct a b _ _ _ ) == (CStruct a' b' _ _ _ ) = (a' == a) && (b' == b)


-- CEnum = CEnum (Maybe Ident)
--    (Maybe [(Ident,             -- variant name
--                   Maybe CExpr)])     -- explicit variant value
--    [CAttr]                     -- __attribute__s
--    NodeInfo
instance Eq CEnum where
  (CEnum a _ _ _) == (CEnum a' _ _ _) = a' == a
  
  
instance Eq (CDeclarationSpecifier a) where 
  (CStorageSpec _)  == (CStorageSpec _) = True
  (CTypeSpec a)     == (CTypeSpec a') = a == a'
  (CTypeQual _)     == (CTypeQual _) = True 
  _ == _ = False

-- Forthe second elem

instance Eq (CDerivedDeclarator a) where
  (CPtrDeclr _  _) == (CPtrDeclr _ _)     = True
  (CArrDeclr _ _ _) == (CArrDeclr _ _ _)  = True
  (CFunDeclr _ _ _) == (CFunDeclr _ _ _)  = True
  _ == _ = False
