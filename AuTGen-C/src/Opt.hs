module Opt where

import System.Console.GetOpt
import Data.Maybe ( fromMaybe ,listToMaybe)
import System.Environment(getArgs, getProgName)
import Types.Option




defaultOptions    = Options
    { optCoverage   = [Decision]
    , optMinBound   = [1]
    , optMaxBound   = [5]
    , targetF       = ""
    , help          = False
    , verbose       = False
    , unitType      = Dependencies
    , unitList      = []
    }

options :: [OptDescr (Options -> Options)]
options =
    [ Option ['d']     ["decision"]
        (ReqArg  selectCoverage "Technique Coverage") "Coverage to be applided. Set to decision <multi,single,token>"
    , Option ['s']     ["start-bound"]
        (ReqArg kMinValue "K-bound") "Inicial bound for unwinding"
    , Option ['m']     ["max-bound"]
        (ReqArg kMaxValue "K-bound") "Max bound for unwinding"
    , Option ['t']     ["target"]
        (ReqArg targetFun "target function") "The target function to coverage"
    , Option ['h']     ["help"]
        (NoArg (\ opts -> opts { help = True }))
        "Help request"
    , Option ['v']     ["verbose"]
        (NoArg (\ opts -> opts { verbose = True }))
        "Outputs more information"
    , Option []     ["uDepend"]
        (NoArg (\ opts -> opts { unitType = Dependencies }))
        "-"    
    , Option []     ["uSelect"]
        (NoArg (\ opts -> opts { unitType = Select }))
        "--"    
    , Option []     ["uSingle"]
        (NoArg (\ opts -> opts { unitType = Single }))
        "--"    
    , Option ['f']     ["function"]
        (ReqArg unitAdd "function") "a function to coverage"
    --, Option ['v']     ["verbose"]
    --    (NoArg (\ opts -> opts { optVerbose = True }))
    --    "chatty output on stderr"
    --, Option ['c']     []
    --    (OptArg ((\ f opts -> opts { optInput = Just f }) . fromMaybe "input")
    --            "FILE")
    --    "input FILE"
    --, Option ['L']     ["libdir"]
    --    (ReqArg (\ d opts -> opts { optLibDirs = optLibDirs opts ++ [d] }) "DIR")
    --    "library directory"
    ]
    
--opts ::[String] -> IO (--)
opts argv = do
    (v,c) <- compilerOpts argv
    if (one c && ( (targetF v) /= "" ) && ( (targetF v) /= "main" ) )
    then return $ to (v,c)
    else if (one c) then error "invalid target function" else error "No file was provided or was provided more then one"
  where
    one [a] = True
    one _ = False 
    to (p1,p2) = (head p2, targetF p1, head $ optCoverage p1, head $  optMinBound p1, head $ optMaxBound p1, verbose p1, unitType p1, unitList p1)

compilerOpts :: [String] -> IO (Options, [String])
compilerOpts argv =
      case getOpt Permute options argv of
         (o,n,[]) -> if (help (values o)) then error (usageInfo header options)
                     else case (valid (values o)) of
                        (True, _) -> return (values o, n)
                        (False, m) -> ioError (userError (info [m]))
         (_,_,errs) -> ioError (userError (info errs))
  where
    values o = foldr id defaultOptions o
    info err = (concat err ++ "\n" ++ usageInfo header options)


-- | error  header
header = "Usage: C-Generator [OPTION...] file\n\tExemple: ./C-Generator -d decision  -t function ./file.c\n"



-- | Check

valid opts = minBoundV opts $ maxBoundV opts $ coverageV opts (True,"")

coverageV opts (b,m) = if( ( length $ optCoverage opts)>2 )
                      then (False, "More then one Coverage Type defined\n"++m)
                      else (b,m)
maxBoundV opts (b,m) = if( ( length $ optMinBound opts)>2 )
                      then (False, "More then one max bound defined\n"++m)
                      else (b,m)
minBoundV opts (b,m) = if( ( length $ optMinBound opts)>2 )
                      then (False, "More then one max bound defined\n"++m)
                      else (b,m)




-- | Function responsable for read the options for type of coverage
selectCoverage "multi" opts = opts { optCoverage   = [DecisionPathSet] ++ (optCoverage opts) } 
selectCoverage "single" opts = opts { optCoverage   = [DecisionVar] ++ (optCoverage opts) }  
selectCoverage "token" opts =  opts { optCoverage   = [Decision] ++ (optCoverage opts) }
selectCoverage _ opts = error "Unknow Coverage. Use decisionPathSet or decisionVar or  decision"

-- |
unitAdd v opts = opts { unitList = (unitList opts) ++ [v] }


-- | Function responsable for read the options for Max k size
kMaxValue k opts = case (maybeRead k) of 
                (Just k') -> opts { optMaxBound = [ k'] ++ optMaxBound opts }
                _ -> error "Max Value is a number?"
                
-- | Function responsable for read the options for Min k size
kMinValue k opts = case (maybeRead k) of 
                (Just k') -> opts { optMinBound = [k'] ++ optMinBound opts }
                _ -> error "Min k Value is a number?"

-- | Function responsable for read the options for select the targe function
targetFun s opts | targetF opts == [] && s /= [] = opts {targetF = s}
                 | otherwise = error "double target function definition"


-- | Aux function to read numbers - 1234a is valid
maybeRead :: String -> Maybe Int
maybeRead = cleanUp . listToMaybe . reads
  where
    cleanUp Nothing = Nothing
    cleanUp (Just (a,b)) = if (b==[]) then Just a
                    else Nothing 



