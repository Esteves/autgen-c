-----------------------------------------------------------------------
{-|
Module      : AutomationI
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental
-}
------------------------------------------------------------------------
module Automation where

import Language.C hiding (pretty, parseC)
import Language.C.System.GCC
--
import Configurations
--
import Text.Printf
import Control.Exception
import System.CPUTime
import qualified Data.Map as M
import qualified Data.List as L
--
import Types.Option
import PreInstrumentation.Automation -- With preInstrumentation set
import Instrumentation.Automation -- With instrumentation set

import qualified TestGeneration.Token.Automation1 as A1-- With TestGeneration set
import TestGeneration.Variables.Automation2 -- With TestGeneration set
import TestGeneration.Variables.Automation3 -- With TestGenerationPath set
--import 


import PathSet.Convert
import Types.CGF
import PathSet.PathSetCreation

import Pretty.Pretty as Draw  --
import Pretty.Test as DrawT  --

import Language.C.Pretty as C

--import Formatting.Clock
import System.Clock

import Data.Maybe as Mb
import Data.List as L

automation :: (FilePath, String, Types.Option.TargetCoverage, Int, Int, Bool, Types.Option.Unit, [String]) -> IO ()
automation (file,targetFunc,coverageType,minBound , maxBound, verbose,uType, uList ) = do
    start <- getCPUTime
    start' <- fmap sec (getTime Monotonic) 

    ast <- processCFile (file)
-- AQUI irá divergir para casos main as target.... TODO iff
    case ast of
      (Left a) -> (putStrLn.show) a
      (Right b) -> do
          astPre <- return $ (preInstrumentation (file)  targetFunc) b
          case coverageType of
            DecisionVar -> do
              (numLoc,astInst) <- return $ instrumentation coverageType targetFunc uType uList astPre
              sAst <- return . show $ Draw.pretty astInst
              writeFile "InstrumentedCode.c" sAst
              (t,failedLocations,_) <- testGeneration verbose targetFunc numLoc astInst minBound maxBound
              writeFile wtestFile $ show $ prettyList t
              
              end' <- fmap sec (getTime Monotonic) 
              let diff' = ( fromIntegral (end' -start'))
              end <- getCPUTime
              let diff = (fromIntegral (end - start)) / (10^12)
              
              --verb verbose (putStrLn sAst)  -- show terminal
              verb verbose (putStrLn $ show $ prettyList t)
              outputHeader diff' diff numLoc ([1..numLoc] L.\\ failedLocations) failedLocations (length t) Nothing
            DecisionPathSet ->do
              (numLoc,astInst) <- return $ instrumentation coverageType targetFunc uType uList astPre
              sAst <- return . show $ Draw.pretty astInst
              mPath' <- return $ gen astInst

              mPath <- case (uType) of
                            Single -> return $ filterMapK [targetFunc] mPath'
                            Select -> return $ filterMapK (uList++[targetFunc]) mPath'
                            Dependencies-> return $ mPath'

              lsPath <- return $ alg (maxBound) (M.findWithDefault [] targetFunc mPath) mPath
              --if(True) then putStrLn ((show mPath) ++ " \n " ++ (show lsPath))
              --         else putStrLn ""
              
              verb verbose (putStrLn $  "Paths:" ++ (show lsPath) ++ "\nNumber: "++(show $ length lsPath )) -- show terminal
              verb verbose (writeFile "path" (show lsPath))
              writeFile "InstrumentedCode.c" sAst
              (t,failedLocations,i) <- testGenerationPath verbose targetFunc numLoc astInst minBound maxBound lsPath
              writeFile wtestFile $ show $ prettyList t
              
              end   <- getCPUTime
              let diff = (fromIntegral (end - start)) / (10^12)
              end' <- fmap sec (getTime Monotonic) 
              let diff' = ( fromIntegral (end' -start'))
              
              --verb verbose (putStrLn sAst) -- show terminal
              verb verbose (putStrLn $  "Number of paths:"++(show $ length lsPath )++ "=>"++ (show lsPath)) -- show terminal
              outputHeader  diff' diff numLoc ([1..numLoc] L.\\ failedLocations) failedLocations (length t) (Just i)
              return ()
------------
            Decision-> do
              (numLoc,astInst) <- return $ instrumentation coverageType targetFunc uType uList astPre
              --sAst <- return . show $ Draw.prettyUsingInclude astInst
              sAst <- return . show $ Draw.pretty astInst
              writeFile "InstrumentedCode.c" sAst
              t <- A1.testGeneration minBound maxBound numLoc
              --putStrLn $ show t
              
              t' <- return $  A1.transformToTest t
              writeFile wtestFile $ show $ prettyList  t'
              
              end <- getCPUTime
              let diff = (fromIntegral (end - start)) / (10^12)
              end' <- fmap sec (getTime Monotonic) 
              let diff' = (( fromIntegral (end' -start')) :: Double )
              
              --verb verbose (putStrLn sAst)
              verb verbose (putStrLn $ show $ prettyList t')
              (locationsFound,failedLocations)<- return $  A1.filterLoc (zip t [1..numLoc]) 
              outputHeader  diff' diff numLoc locationsFound failedLocations (length locationsFound) Nothing
              return ()
            otherwise -> putStrLn "?? This is for future" >> return ()
       
            
-- >> UTILE << --
outputHeader
   :: (Integral a1, Show a1, Show a2, Show a3) => 
     Double -> Double -> a1 -> [a] -> a3 -> a2 -> Maybe Int ->IO ()
outputHeader diff' diff numberOfLocations locationsFound failedLocations numberOfTests mayNumPathsFailed  = do
    printf    "CPU computation time: %0.4f sec\n" (diff :: Double)
    diff''<-  if(diff' > 60) then return (diff'/ 60)
              else return diff'
    if (diff' <= 60) then  printf    "Total computation time: %0.0f sec\n" (diff'':: Double)
      else  printf "Total computation time: %0.0f min \n" (diff'':: Double)
    putStrLn  ("Number of Locations to coverage: " ++  (show numberOfLocations))
    putStrLn  ("Coverage achieved: " ++ show value ++"%")
    putStrLn  $ "Number of Tests: " ++ ( show numberOfTests)
    putStrLn  $ "Failed locations:" ++ (show failedLocations) 
    case mayNumPathsFailed of
      Nothing -> return ()
      (Just i) -> putStrLn  $ "Number of infeasible paths set tested: " ++ (show i)
  where 
     value = ((fromIntegral (length locationsFound))  / (fromIntegral numberOfLocations) *100) 

processCFile file = cfile Nothing file

cfile Nothing s = parseCFile (newGCC "gcc") (Just "./") [] s
cfile p s = parseCFile (newGCC "gcc") p [] s


verb bool g = if (bool) then g else return ()

--filterMapK ls m =  Mb.fromMaybe M.empty $ M.traverseWithKey   (\k v ->  if (0==( length $ L.filter (k==) ls )) then Just [] else Just v) m
filterMapK ls mp =   M.filterWithKey (\k e-> any(\k1 -> k==k1) ls )   mp 
