-----------------------------------------------------------------------
{-|
Module      : Automation of BMC
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental

-}
------------------------------------------------------------------------
module TestGeneration.Variables.Automation2 (testGeneration)where

import Control.Monad.State.Lazy
import qualified Data.Set as S
import Language.C.Syntax.AST

import Types.AST
import TestGeneration.Variables.Test


data Struct = Struct {
  target  :: String,
  locals  :: S.Set Int,
  kMin    :: Int,
  kMax    :: Int,
  tests   :: [S.Set (String, String, String)],
  ast     :: CTranslUnitAnnot,
  failLocals :: S.Set Int,
  verbose :: Bool
} deriving (Show)

--empty = Struct {target = "", locals = S.empty, tests=[], ast = CTranslUnit [] undefAnnot, kMin = 0, kMax = 0, failLocals = S.empty}

testGeneration bool target i ast k max = evalStateT (generation >> result i) (Struct target (S.fromDistinctAscList [1..i]) k max [] ast S.empty bool)
-- eval com seleção do resultado

result i = get >>= (\s-> return (map S.toAscList $ tests s, S.toAscList $ failLocals s,i) )

generation :: StateT Struct IO ()
generation = do 
    s<- get
    let lc = locals s
    if ((0==) $ S.size lc) then return ()
    else 
      do
        l <- select
        (ok,t,lr) <- liftIO $ test (target s) l (ast s) (kMin s) (kMax s)
        if (verbose s) then
            liftIO (putStrLn $ (show l) ++ "    " ++ (show t) ++ "    " ++ show lr)
        else return ()
        case ok of
          True ->  do -- Exist a test 
            modify (const s {locals = S.difference(locals s) lr, tests = t : (tests s)  } )
          False -> do
            modify (const s {failLocals = S.union (S.fromDistinctAscList l) (failLocals s) ,locals = S.difference (locals s) (S.fromDistinctAscList l)} )
        generation 


select :: StateT Struct IO [Int]
select = do 
    s <- gets locals 
    return . single  $ S.findMax s

single a = [a]

fromMaybe a Nothing = a
fromMaybe _ (Just a) = a
