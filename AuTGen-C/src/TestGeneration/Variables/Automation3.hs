-----------------------------------------------------------------------
{-|
Module      : Automation of BMC
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental

-}
------------------------------------------------------------------------
module TestGeneration.Variables.Automation3 (testGenerationPath)where

import Control.Monad.State.Lazy
import qualified Data.Set as S
import Language.C.Syntax.AST

import Types.AST
import TestGeneration.Variables.Test
import Types.Paths


data Struct = Struct {
  target  :: String,
  locals  :: S.Set Int,
  kMin    :: Int,
  kMax    :: Int,
  tests   :: [S.Set (String, String, String)],
  ast     :: CTranslUnitAnnot,
  paths :: Paths,
  verbose :: Bool,
  failpaths :: Int
} deriving (Show)


testGenerationPath bool target i ast k max paths = do
      let local = (S.fromDistinctAscList [1..i])
      evalStateT (generation >> result) (Struct target local k max [] ast (createPaths paths) bool 0)
-- eval com seleção do resultado

result = get >>= (\s-> return (map S.toAscList $ tests s, S.toAscList $ locals s,failpaths s) )

generation :: StateT Struct IO ()
generation = do 
    s<- get
    let lc = locals s
    let cp = paths s
    if (   ((S.size lc) == 0) ||  (isEmpty cp)  ) then return ()
    else 
      do
        l <- select
        (ok,t,lr) <- liftIO $ test (target s) (S.toAscList l) (ast s) (kMin s) (kMax s)
        if (verbose s)  then
          liftIO  (putStrLn $ " Selected: "++(show l) ++ " \n\tWas achieved => " ++ (show lr))
        else return ()
        case ok of
          True ->  do -- Exist a test 
            modify (const s {locals = S.difference(locals s) lr, tests = t : (tests s), paths= update lr cp  } )
            if (verbose s)  then do
              pp <- gets paths
              liftIO ( putStrLn  ("\tPaths Updated To:"++ (show pp) ++ "\n"))
             else return ()
          False -> do
            if (verbose s)  then do
              liftIO ( putStrLn  (""))
              else return ()
            modify (const s {paths = remove l (cp) , failpaths = failpaths s + 1})
        generation 


select :: StateT Struct IO Path
select = do 
    s <- gets paths 
    return  $ greaterPath s


update :: Path -> [Path]->[Path]
update = rmLocals
