-----------------------------------------------------------------------
-- |
-- Module      : Automation of BMC
-- Description : 
-- Copyright   : (c) Fábio Esteves Sousa, 2014
-- License     : 
-- Maintainer  : 
-- Stability   : experimental
--
--
--
------------------------------------------------------------------------

module TestGeneration.Variables.Test (test) where

import Control.Monad.IO.Class
import Control.Monad.State
import System.Exit
import System.Process
import TestGeneration.Variables.Statements
import Types.AST
import Configurations
import Pretty.Pretty as Draw
import XML.XmlCBMC
import TestGeneration.TraceParse (splitVarAndLocations)

import Data.Set

data Struct = Struct {
   oK      :: Bool
  ,kMin    :: Int
  ,kMax    :: Int
} deriving (Show)



test
  ::
   String
  -> [Int] -- ^ elementos a atingir
  -> CTranslUnitAnnot -- ^ ast pre Instrumented
  -> Int 
  -> Int
  -> IO (Bool, Set (String, String, String), Set Int)
test s ls ast k m = evalStateT (setStatments s ls ast >> untilTest >>= setResult ) (Struct False k m) 


setStatments :: String -> [Int] -> CTranslUnitAnnot -> StateT Struct IO ()
setStatments s r ast = liftIO $ writeFile (fileSructRun) f -- >>  liftIO (putStrLn f)
    where 
      f =  (show . Draw.pretty) $ generateFor s r ast


--generateFor r (ast st)}
untilTest :: StateT Struct IO [CBMCxml]
untilTest = do 
      st <-get 
      let  ok = oK st
      let  k = kMin st
      let  max = kMax st

      if( and [ not ok, k <= max ] )
      then do 
           (e,o,_) <-liftIO $  runCBMC k
           if (not ( (e == ExitSuccess)  || (e == ExitFailure 10) )) then raise "CBMC run fail. Check file being test"
           else do
                r <- liftIO $ xmlStrTrace o
                case r of
                  [] -> raise "Error while parsing  in XML"
                  [(CBMCxml False _ )] -> return r
                  [(CBMCxml True _ )] -> modify (const  (Struct ok (k+1) max)) >> untilTest
      else return []


setResult :: [CBMCxml] -> StateT Struct IO ( Bool , Set ([Char], [Char], String), Set Int )
setResult ((CBMCxml False o ):[]) = return . (\(x,y) -> (True,x,y)) $ splitVarAndLocations o
setResult _ = return (False, empty ,empty)


runCBMC k = readProcessWithExitCode "cbmc" [ fileSructRun , "--unwind", (show k), "--no-unwinding-assertions", "--xml-ui", "--property", "main.assertion.1"] []

raise = liftIO . ioError . userError

