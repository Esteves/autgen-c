{-# LANGUAGE FlexibleInstances #-}

-----------------------------------------------------------------------
{-|
Module      : Automation of BMC
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental


This is responsible for put the necessary properties  in all exit of the target function. what is very stupid  as is call by another function, the main,witch  could contain the necessary properties after the target function be call. 
TODO change it 

-}
------------------------------------------------------------------------
module TestGeneration.Variables.Statements (generateFor) where 

import Language.C.Syntax.AST
import Language.C.Data.Ident
import Control.Monad.State
import Language.C.Syntax.Constants
import Language.C.Data.Node
--import Language.C.Data.Ident
--import Control.Monad.Identity
--import Language.C.Syntax.Constants
--
import Configurations
import Types.Annotations
import Types.AST
--

generateFor :: String -> [Int] -> CTranslUnitAnnot -> CTranslUnitAnnot
generateFor s v ast = evalState (convert ast)  ((map assume v) ++ [assert], "main") 

-- aux --
getIdentName (CFunDef _ (CDeclr (Just (Ident s _ _))_ _ _ _) _ _ _) = s
getIdentName _ = ""
--


assume i = Assume (CBinary CEqOp (CVar (Ident (location ++ (show i)) 0 undefNode ) undefAnnot) (CConst (CIntConst (cInteger 1) undefAnnot)) undefAnnot)

assert = Assert (CConst (CIntConst (cInteger 0) undefAnnot))


class Statement p where
    -- |
    convert :: p -> State ([Note],String) p
 -- ---------------------------------------- --  
 -- ---------------------------------------- --

instance Statement (CTranslationUnit Annot) where
    convert (CTranslUnit a b) = do  a'<- mapM convert a
                                    return $ CTranslUnit a' b
    -- ^ CTranslUnit [CExternalDeclaration a] a

instance Statement (CExternalDeclaration Annot) where
   convert (CFDefExt a) = do 
      n <- gets snd
      if ((getIdentName a) == n ) 
        then do 
          a' <- convert a
          return $ CFDefExt a'
        else return (CFDefExt a)
   -- ^ CFDefExt (CFunctionDef a)
   convert a = return a 
   -- ^ CAsmExt (CStringLiteral a) a
   -- ^ CDeclExt (CDeclaration a)

instance Statement (CFunctionDef Annot) where
    convert (CFunDef a b c d e) = do 
        d' <- convert d 
        return  $ CFunDef a b c d' e
   -- ^ CFunDef [CDeclarationSpecifier a] (CDeclarator a) [CDeclaration a]  (CStatement a) a


instance Statement (CStatement Annot) where
        convert (CCompound a b c ) = do
          s <- gets fst
          r <- return $ CReturn  Nothing $ preUpdata (++s) undefAnnot
          return $ CCompound a ( b ++ [CBlockStmt r] )  c
  -- ^ CCompound [Ident] [CCompoundBlockItem a] a

