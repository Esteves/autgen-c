-----------------------------------------------------------------------
{-|
Module      : Automation of BMC
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental
-}
------------------------------------------------------------------------
module TestGeneration.Token.Automation1 where
--
import XML.XmlCBMC
import TestGeneration.TraceParse (splitVarAndLocations)
--
import System.Exit
import System.Process
import Control.Applicative
import Data.List
import Pretty.Test
import qualified Data.Set as S
--

-- TODO   name file to be write as argument

runCBMC tag k = readProcessWithExitCode "cbmc" ["-D", "DEFINE_"++(show tag), "InstrumentedCode.c", "--unwind", (show k), "--no-unwinding-assertions","--xml-ui"] []

oneTestGeneation k maxK tag = do
		r <- runCBMC tag k 
		case r of 
			(ExitFailure 6 ,r2,_) -> error "file to CBMC not found"
			(_,out, _) -> do
						v <- xmlStrTrace out
						case v of
							[] -> error "Error while parsing  in XML"
							[(CBMCxml False _ )] -> return v
							[(CBMCxml True _ )] -> if (k > maxK)  then  (return v) else oneTestGeneation (k+1) maxK tag

testGeneration minK maxK maxD  = (sequence $ map (oneTestGeneation  minK maxK)  [1..(maxD)]) >>= return.concat
-- NoteforFutureImplementation - If oneTestGeneation instead of error return [] or nothing and just to know were was not found because of bug.

--
--
-- data CBMCxml = CBMCxml { getStatus :: Bool, -- ^ Prover Status, if was found a violation.
   --                      getTrace::[(String,String,String)] -

--testValidation :: [CBMCxml] -> [CBMCxml]
testValidation ls = map getTrace $ filter ((False==).getStatus) ls

filterLoc [] = ([],[])
filterLoc ((a,b) :ls) | (False==) $ getStatus a = add1 b (filterLoc ls)
                      | (True==)  $ getStatus a = add2 b (filterLoc ls)
  where
    add1 w (xs,ys) = (w:xs, ys) 
    add2 w (xs,ys) = (xs  , w:ys) 

transformToTest x  = map (S.toAscList . fst)  $ map  splitVarAndLocations (testValidation  x)  
  

