-----------------------------------------------------------------------
-- |
-- Module      : Automation of BMC
-- Description : 
-- Copyright   : (c) Fábio Esteves Sousa, 2014
-- License     : 
-- Maintainer  : 
-- Stability   : experimental
--
--
--
------------------------------------------------------------------------

module TestGeneration.TraceParse where

--import Control.Monad.IO.Class
--import Control.Monad.State
--import System.Exit
--import System.Process
--import TestGeneration.Variables.Statements
--import Types.AST
--import Configurations
--import Pretty.Pretty as Draw

import Data.List 
import Data.Set as Set (insert, notMember, empty,fromList,toList)

import XML.XmlCBMC
import Pretty.Test
import Configurations

data V = V (String,String,String)

instance Eq V where
  (==) (V (a,b,c)) (V (d,e,f)) = a == d
instance Ord V where
  compare (V (a,b,c)) (V (d,e,f)) = compare a d
  
toV :: (String,String,String) -> V
toV x = V x

fromV (V x) = x 

  
partitionT app1 app2 p1 p2 s xs = foldr (selectT app1 app2 p1 p2) s xs

selectT app1 app2 p1 p2 x ~(ts,fs) | p1 x       = (app1 x ts,fs)
                                   | p2 x       = (ts         ,app2 x fs)
                                   | otherwise = (ts,fs)

splitVarAndLocations xs =  {-error $ show xs-} fmap1 clean $ partitionT app1 app2 p1 p2 (empty,empty) xs 
  where
      app1 = (\x s -> addTo2 (toV (removeTag id ("main::1::"++prefix) x)) s)
      p1 = (\(x,_,_) ->  ( not(isPrefixOf ("main::1::"++prefix++infix_) x) && (isPrefixOf ("main::1::"++prefix) x)) )
      app2 = (\x s->  addTo (removeTag readIt location x) s)
      p2 = (\(x,_,_) -> isPrefixOf location x)
      fmap1 f (x,y) = (f x, y)
      clean = Set.fromList . map fromV . Set.toList

-- | Resposable for obtaining the location achived filter
-- To avoid creation of type with objective to impose special ord  for the Set.insert function, I use this as filter to decide if is to insert
addTo x@(a,b,c) s | b=="1" && notMember a s = Set.insert a s
                  | otherwise = s

addTo2 x@(V (a,b,c)) s| notMember x s = Set.insert x s
                      | otherwise = s

removeTag t s = (\(x,y,z)-> (   t      $ fromMaybe "" (stripPrefix s x),y,z) )

readIt = (\(x,y)->x). head.(reads :: String -> [(Int, String)])

fromMaybe a Nothing = a
fromMaybe _ (Just a) = a
