-----------------------------------------------------------------------
{-|
Module      : Automation
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental
-}
------------------------------------------------------------------------
module Instrumentation.Automation where


import Types.Annotations

import Instrumentation.Coverage.StatementDecisionCoverage
import qualified Instrumentation.Coverage.StatementDecisionCoverageToken as T

import PreInstrumentation.FunctionDependencies
import PreInstrumentation.Variables.LocationVars
import Types.Option

import Language.C
import Language.C.Data.Ident
import Control.Monad.Identity


instrumentation typeToApp s u ulist  =  (\x -> runIdentity (inst typeToApp s u ulist x))

inst typeToApp s u ulist ast
  | (typeToApp == DecisionVar) || (DecisionPathSet == typeToApp) = do
      newAst <- return $ switchToAnnot ast
      idents' <- return $ getFunDependenciesOf s ast
      idents <- case u of
          Single-> return $ reduceIdents [s] idents'
          Select ->  return $ reduceIdents ([s] ++ ulist) idents'
          Dependencies -> return idents'
      (i,newAstPre) <- return $ coverage idents newAst 
      newAstPos <-return $ locationInitVars 1 i newAstPre
      return $ (i,newAstPos)
--      error (show idents )
  | (typeToApp == Decision) = do
      newAst <- return $ switchToAnnot ast
      idents' <- return $ getFunDependenciesOf s ast
      idents <- case u of
          Single-> return $ reduceIdents [s] idents'
          Select ->  return $ reduceIdents ([s] ++ ulist) idents'
          Dependencies -> return idents'
      return $ T.coverage idents newAst

  
  | otherwise = error "This is for future impleemtation? (Instrumentation.Automation)"
  


switchToAnnot :: (CTranslationUnit NodeInfo) -> CTranslationUnit Annot
switchToAnnot = (fmap (\x-> ([],[],[],x)::Annot ))



reduceIdents ls idents = filter (\x-> elemF  identEq x ls)  idents

identEq (Ident s' _ _) s = (s == s')

elemF f _ []       = False
elemF f x (y:ys)   = (f x y) || elemF f x ys

    
    
    
    
    
    
