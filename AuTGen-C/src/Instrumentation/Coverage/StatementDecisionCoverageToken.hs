{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
-----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
--Um módulo contendo
-----------------------------------------------------------------------------

module Instrumentation.Coverage.StatementDecisionCoverageToken where
--
import Types.Annotations
--
import Control.Monad.State
import Language.C.Syntax.AST
import Language.C.Data.Node
import Language.C.Syntax.Constants (cInteger, getCInteger) 
import Language.C.Data.Position (nopos)
import Language.C.Data.Ident
import Data.List (elem)
--

type StatementState = (Int, [Ident])
  -- ^  1) Number +1 of assertation  need to be cheack
  --    2) List of functions to be asserted
coverage_ :: [Ident] -> (CTranslationUnit Annot) -> (CTranslationUnit Annot)
coverage_ ls = (\x-> evalState (convert x) (1,ls))

coverage :: [Ident] -> (CTranslationUnit Annot) -> (Int,(CTranslationUnit Annot))
coverage ls = select . (\x-> runState (convert x) (1,ls))
  where
   select (a,(b,c)) =  (b-1,a) 
   -- ^ uncarry of this get  Number and de Ast

getIdent (CFunDef _ (CDeclr (Just s)_ _ _ _) _ _ _) = s
getIdent _ = Ident "" 0 undefNode


class Statement p where
    -- |
    convert :: p -> State StatementState p
 -- ---------------------------------------- --  
 -- ---------------------------------------- --
 
instance Statement (CTranslationUnit Annot) where
    convert (CTranslUnit a b) = do  a'<- mapM convert a
                                    return $ CTranslUnit a' b
    -- ^ CTranslUnit [CExternalDeclaration a] a

instance Statement (CExternalDeclaration Annot) where
   convert (CDeclExt a) = return $ CDeclExt a
   -- ^ CDeclExt (CDeclaration a)
   convert (CFDefExt a) = do 
      
      ls <-gets (snd)
      if (elem (getIdent a) ls) 
        then do 
          a' <- convert a
          return $ CFDefExt a'
        else return (CFDefExt a)
   -- ^ CFDefExt (CFunctionDef a)
   convert (CAsmExt a b) = convert a >>= \x -> return $ CAsmExt x b
   -- ^ CAsmExt (CStringLiteral a) a

instance Statement (CFunctionDef Annot) where
    convert (CFunDef a b c d e) = do 
        i <- gets (fst)
        modify (pair (+1))
        a' <- mapM convert a  
        b' <- convert b  
        c' <- mapM convert c 
        d' <- convert d 
        return  $ CFunDef a' b' c' d'  ([],[assert i],[], nodeInfo e)
   -- ^ CFunDef [CDeclarationSpecifier a] (CDeclarator a) [CDeclaration a]  (CStatement a) a

instance Statement (CDeclaration Annot) where
    convert x@(CDecl a b c) = do
        a' <- mapM convert a
        b' <- mapM func b
        return $ CDecl a' b' c
      where func = liftT3 f1 f2 f3 
         -- ^ Is not possible to reduce. Is different functions  with same name
            f1 = maybe (return Nothing )  (((liftM) (Just)) . convert)
            f2 = maybe (return Nothing )  (((liftM) (Just)) . convert)
            f3 = maybe (return Nothing )  (((liftM) (Just)) . convert)                      
  -- ^ CDecl [CDeclarationSpecifier a] [(Maybe (CDeclarator a), Maybe (CInitializer a), Maybe (CExpression a))] a


instance Statement (CDeclarator Annot) where
    convert a = return a                      
   -- ^ CDeclr (Maybe Ident) [CDerivedDeclarator a] (Maybe (CStringLiteral a)) [CAttribute a] a
   
-- TODO remove this   
--  convert (CDeclr a b c d e) = do  a' <- convert a
--                                   b' <- mapM convert b
--                                   c' <- maybe (return Nothing) convert c
--                                   d' <- mapM (convert) d
--                                   return a' b' c' d' e
--  


instance Statement (CDerivedDeclarator Annot) where
    convert a = return a
   -- ^ CFunDeclr (Either [Ident] ([CDeclaration a],Bool)) [CAttribute a] a
   -- ^ CPtrDeclr [CTypeQualifier a] a
   -- ^ CArrDeclr [CTypeQualifier a] (CArraySize a) a

-- TODO remove this
--  convert (CFunDeclr a  b c ) = do  a' <- either (mapM convert) (pair (mapM convert) id) a
--                                    b' <- mapM convert b
--                                    return $  CFunDeclr a b c                            
--convert (CPtrDeclr a b) = return $ (CPtrDeclr a b)
--convert (CArrDeclr a (CArraySize a) a) = return 



instance Statement (CArraySize Annot) where
   convert a = return a
   -- ^ CNoArrSize Bool 
   -- ^ CArrSize Bool (CExpression a) 

instance Statement (CStatement Annot) where
    convert (CIf a b Nothing d ) = do 
          i<- gets (fst )
          modify (pair (+1))
          b' <- convert b
          ii <- gets(fst)
          modify (pair (+1))
          return $ CIf a b' Nothing  ([],[assert i],[assert (ii)], nodeInfo d)
    convert (CIf a b (Just c) d ) = do  
          i <- gets (fst )
          modify (pair (+1))
          b' <- convert b
          c' <- convert c
          ii <- gets (fst )
          modify (pair (+1))
          return $ CIf a b' (Just c')  ([],[assert i],[assert (ii)], nodeInfo d) 
   -- ^ CIf (CExpression a) (CStatement a) (Maybe (CStatement a)) a
    convert (CCase a b c) = do 
          i <- gets (fst )
          modify (pair (+1))
          b' <- convert b
          return $ CCase a b' ([], [assert i], [],nodeInfo c)        
   -- ^ CCase (CExpression a) (CStatement a) a
    convert (CCases a b c d) = do 
          i <- gets (fst )
          modify (pair (+1))
          b' <- convert b
          c' <- convert c
          return $  CCases a b' c' ([], [assert i], [],nodeInfo d)           
   -- ^ CCases (CExpression a) (CExpression a) (CStatement a) a {- gcc implements an extension to the C language case 1...4: is valid -}

--TODO CCompound CCompoundBlockItem
    convert (CLabel a b c d) = do
          b' <- convert b
          c' <- mapM convert c
          return $ CLabel a b' c' d 
   -- ^ CLabel Ident (CStatement a) [CAttribute a] a
    convert (CDefault a b ) = do
          i <- gets (fst )
          modify (pair (+1))
          a' <- convert a
          return $ CDefault a' ([], [assert i], [],nodeInfo b)
   -- ^ CDefault (CStatement a) a
    convert (CCompound a b c ) = do
          b' <- mapM convert b 
          return $ CCompound a b' c
   -- ^ CCompound [Ident] [CCompoundBlockItem a] a
    convert (CWhile a b c d) = do
          ii <- gets (fst )
          modify (pair (+1))
          i <- gets (fst )
          modify (pair (+1))
          a' <- convert a
          b' <- convert b 
          return $ CWhile a' b' c ( [],[assert i],[assert (ii)], OnlyPos nopos (nopos,-1) )
   -- ^ CWhile (CExpression a) (CStatement a) Bool a
    convert (CFor a b c d e) = do
          a' <- eitherM (maybeM_ convert)  convert a 
          ii <- gets (fst )
          modify (pair (+1))
          i <- gets (fst )
          modify (pair (+1))
          b' <- maybeM_ convert b
          c' <- maybeM_ convert c
          d' <- convert d
          return $ CFor a' b' c' d' ( [],[assert i],[assert (ii)], OnlyPos nopos (nopos,-1) )
   -- ^ CFor (Either (Maybe (CExpression a)) (CDeclaration a)) (Maybe (CExpression a)) (Maybe (CExpression a)) (CStatement a) a
    convert (CSwitch a b c) = do
          a' <- convert a 
          b' <- convert (setDefault b) 
          return $ CSwitch a' b' c
 -- ^ CSwitch (CExpression a) (CStatement a) a 

    convert a = return a  
   -- ^ CExpr (Maybe (CExpression a)) a
   -- ^ CGoto Ident a
   -- ^ CGotoPtr (CExpression a) a
   -- ^ CCont a
   -- ^ CBreak a
   -- ^ CAsm (CAssemblyStatement a) a
   -- ^ CReturn (Maybe (CExpression a)) a

instance Statement (CAssemblyStatement Annot) where
   convert a = return a
   -- ^ CAsmStmt

instance Statement (CAssemblyOperand Annot) where
   convert a = return a
   -- ^ CAsmOperand

instance Statement (CCompoundBlockItem Annot) where
    convert (CBlockStmt a) = do
        a' <- convert a
        return $ CBlockStmt a'      
   -- ^ CBlockStmt (CStatement a) 
    convert (CBlockDecl a) = do 
        a' <- convert a
        return $  CBlockDecl a'      
   -- ^ CBlockDecl (CDeclaration a)
    convert (CNestedFunDef a) = do 
        a' <- convert a
        return $ CNestedFunDef a'          
   -- ^ CNestedFunDef (CFunctionDef a) 

instance Statement (CDeclarationSpecifier Annot) where
   convert (CStorageSpec a) = do
          a' <- convert a
          return $ CStorageSpec a'      
   -- ^ CStorageSpec (CStorageSpecifier a) 
   convert (CTypeSpec a) = do
          a' <- convert a
          return $ CTypeSpec a'      
   -- ^ CTypeSpec (CTypeSpecifier a) 
   convert (CTypeQual a) = do
          a' <- convert a
          return $ CTypeQual a'      
   -- ^ CTypeQual (CTypeQualifier a) 

instance Statement (CStorageSpecifier Annot) where
    convert a = return a
   -- ^ CAuto a 
   -- ^ CRegister a 
   -- ^ CStatic a 
   -- ^ CExtern a 
   -- ^ CTypedef a 
   -- ^ CThread a 

instance Statement (CTypeSpecifier Annot) where
    convert a = return a
   -- ^ CVoidType a
   -- ^ CCharType a
   -- ^ CShortType a
   -- ^ CIntType a
   -- ^ CLongType a
   -- ^ CFloatType a
   -- ^ CDoubleType a
   -- ^ CSignedType a
   -- ^ CUnsigType a
   -- ^ CBoolType a
   -- ^ CComplexType a
   -- ^ CSUType (CStructureUnion a) a 
   -- ^ CEnumType (CEnumeration a) a 
   -- ^ CTypeDef Ident a 
   -- ^ CTypeOfExpr (CExpression a) a 
   -- ^ CTypeOfType (CDeclaration a) a 

instance Statement (CTypeQualifier Annot) where
    convert a = return a
   -- ^ CConstQual a
   -- ^ CVolatQual a
   -- ^ CRestrQual a
   -- ^ CInlineQual a
   -- ^ CAttrQual (CAttribute a)

instance Statement (CStructureUnion Annot) where
    convert a = return a
   -- ^ CStruct
   
instance Statement(CStructTag) where
    convert a = return a
  -- ^ CUnionTag
  -- ^ CStructTag

instance Statement (CEnumeration Annot) where
    convert a = return a
   -- ^ CEnum (Maybe Ident) (Maybe [(Ident, Maybe (CExpression a))]) [CAttribute a]
            

instance Statement (CInitializer Annot) where
    convert a = return a
   -- ^ CInitExpr (CExpression a) a
   -- ^ CInitList (CInitializerList a) a
   
instance Statement (CInitializerList Annot) where
    convert a = return a
   -- ^ [([CPartDesignator a], CInitializer a)] 
instance Statement (CPartDesignator Annot) where
    convert a = return a
   -- ^ CArrDesig (CExpression a) a
   -- ^ CMemberDesig Ident a
   -- ^ CRangeDesig (CExpression a) (CExpression a) a
   
instance Statement (CAttribute Annot) where
    convert a = return a
    -- ^CAttr Ident [CExpression a] a

instance Statement (CExpression Annot) where
    convert (CStatExpr a b) = do
          a' <- convert a
          return $ CStatExpr a' b
   -- ^ CStatExpr (CStatement a) a 
    convert a = return a
   -- ^ CComma [CExpression a] 
   -- ^ CAssign CAssignOp 
   -- ^ CCond (CExpression a) 
   -- ^ CBinary CBinaryOp 
   -- ^ CCast (CDeclaration a) 
   -- ^ CUnary CUnaryOp 
   -- ^ CSizeofExpr (CExpression a)
   -- ^ CSizeofType (CDeclaration a) 
   -- ^ CAlignofExpr (CExpression a)
   -- ^ CAlignofType (CDeclaration a) 
   -- ^ CComplexReal (CExpression a) 
   -- ^ CComplexImag (CExpression a) 
   -- ^ CIndex (CExpression a) 
   -- ^ CCall (CExpression a) 
   -- ^ CMember (CExpression a) 
   -- ^ CVar Ident 
   -- ^ CConst (CConstant a) 
   -- ^ CCompoundLit (CDeclaration a)
   -- ^ CLabAddrExpr Ident a 
   -- ^ CBuiltinExpr (CBuiltinThing a) 

instance Statement (CBuiltinThing Annot) where
    convert a = return a
   -- ^ CBuiltinVaArg (CExpression a) (CDeclaration a) a 
   -- ^ CBuiltinOffsetOf (CDeclaration a) [CPartDesignator a] a 
   -- ^ CBuiltinTypesCompatible (CDeclaration a) (CDeclaration a) a 

instance Statement (CConstant Annot) where
    convert a = return a
   -- ^ CIntConst CInteger a
   -- ^ CCharConst CChar a
   -- ^ CFloatConst CFloat a
   -- ^ CStrConst CString a


instance Statement (CStringLiteral Annot) where
  convert a = return a
    -- ^ CStrLit CString a

 
-----------------------------------------------------------------------------
--------------------- UTIL --------------------------------------------------
liftT3 f g h (a,b,c) = do a' <- f a
                          b' <- g b
                          c' <- h c
                          return (a',b',c')
                          
                          
maybeM_ f =  maybe (return Nothing )  (((liftM) (Just)) . f)                          
eitherM f g = either ((liftM Left). f ) ((liftM Right). g )                          
                          
empty = ([],[],[], OnlyPos nopos (nopos,-1) )
                          
zero = cConst  0
cConst i = (CConst (CIntConst (cInteger i) empty ) )                          
assert i = IFDef i [Assert zero]
pair f (a,b) = (f a , b)

setDefault (CCompound a b c) =  if ((length $ (filter isDefault b) ) == 0)
      then (CCompound a (b ++ [newV]) c)
      else (CCompound a b c)
  where
    newV = (CBlockStmt (CDefault (CExpr Nothing empty) empty)) 

isDefault (CBlockStmt (CDefault _ _) ) = True
isDefault _ = False 
