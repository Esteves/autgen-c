{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, FlexibleContexts #-}
-----------------------------------------------------------------------------
-- |
-- Module      :  Language.C.Pretty
-- Copyright   :  Copyright (c) 2007 Bertram Felgenhauer
--                          (c) 2008 Benedikt Huber
-- License     :  BSD-style
-- Maintainer  : benedikt.huber@gmail.com
-- Stability   : experimental
-- Portability : portable
--
-- This module provides a pretty printer for the parse tree
-- ('Language.C.Syntax.AST').
-----------------------------------------------------------------------------
module Pretty.Test (
    -- * Pretty Printing
    prettyList
) where

import Text.PrettyPrint.HughesPJ

-- | A class of types which can be pretty printed
class Pretty p where
    -- | pretty print the given value
    pretty     :: p -> Doc

instance Pretty (String,String,String) where 
    pretty (a,b,c) = text c <+> text a <+> text b

instance (Pretty a) => Pretty [a] where
    pretty [] = empty 
    pretty [a] = pretty a 
    pretty (l:ls) = pretty l <> foldl  (\x y -> x <> text "," <+> pretty y )  empty ls
    --foldr connect  ( pretty $ head ls) $ map pretty (tail ls)
      where
        
        
prettyList [] = empty
prettyList ls = foldr (\x y -> (pretty x <> text ";") $$ y) empty ls

