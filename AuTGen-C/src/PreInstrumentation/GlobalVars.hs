{-# LANGUAGE FlexibleInstances #-}
----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
--Um módulo contendo
-----------------------------------------------------------------------------
module PreInstrumentation.GlobalVars (
  getGlobalVars, declarToPreInstVar,removeConstVars

) where
--
import PreInstrumentation.Util
--
import Language.C.Data.Ident
import Language.C.Syntax.AST
import Language.C.Data.Node
import Data.Maybe
--
declarToPreInstVar :: [CDeclaration ()] -> PreInstrumentedVar
declarToPreInstVar = globaldepend

getGlobalVars s = globaldepend . filter isD . cleanUp . select s  . getT 
  where 
    select s2 = filter ( (equals s2). pathFile) 
    cleanUp   = fmap (fmap (\x-> () ))
    --
    pathFile  = fileOfNode . annotation
    equals s1 = (fromMaybe False) . (fmap(== s1))
    isD (CDeclExt _) = True
    isD _ = False

-- | Class 
class GlobalDependacie c where
  globaldepend :: c -> PreInstrumentedVar

instance GlobalDependacie a => GlobalDependacie  [a] where
  globaldepend = concat . (fmap globaldepend)

instance GlobalDependacie (CExternalDeclaration ()) where 
  globaldepend (CDeclExt a) = globaldepend a
  globaldepend _ = []

instance GlobalDependacie (CDeclaration ()) where
  globaldepend (CDecl a b _) =map (put a) . cleanUp globaldepend $ cleanUp removeNoIdent b -- ^ b has type [(Maybe (CDeclarator a), Maybe (CInitializer a), Maybe (CExpression a))] 
   where
    cleanUp = (\x->concat . (map x))
    removeNoIdent ((Just a),_,_) = [a]
    removeNoIdent _ =[]
    put b (a,_,c) =(a,b,c)

instance GlobalDependacie (CDeclarator()) where
  globaldepend (CDeclr (Just a) b _ _ _) = [(a,[],b)]
  globaldepend _ = []

-- END Class


removeConstVars ::PreInstrumentedVar->PreInstrumentedVar
removeConstVars = filter app
 where
    app (_,b,c)= not $ and_ (isL isConst b) (not (isL isPointer c))
    and_ a b = and [a,b]

isConst :: CDeclarationSpecifier t -> Bool
isConst (CTypeQual (CConstQual a)) = True
isConst _ = False
--
isPointer (CPtrDeclr _ _) = True
isPointer _ = False
--
isL x = (or.(map x))
