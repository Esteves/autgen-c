----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
--Um módulo contendo
-----------------------------------------------------------------------------
module PreInstrumentation.Statements (
  -- * Main Function Construction
  addMain, reName,
  -- * Declarations
  -- ** Static Declarations 
  mainHead,
  -- ** Simple Declarations
  charDecl,intDecl,
  -- ** Remaning Declarations 
  declare,cleanNoSizeAttr,
  -- ** Location Variable Declarations 
  specLoc,
  -- * Atributions
  setRandInt, setRandChar,
  -- * Assigments
  toAssign,
  -- * Function Call
  callFun
)where
--
import Configurations
import PreInstrumentation.Util
--
import Language.C.Syntax.AST
import Language.C.Data.Node
import Language.C.Data.Ident
import Control.Monad.Identity
import Language.C.Syntax.Constants

import PreInstrumentation.StatementsAssiments
import PreInstrumentation.StatementsDeclarations
import PreInstrumentation.StatementsUtill


import Control.Monad.State
import Control.Applicative 

-------------------------------
--  Main Function Construction
-------------------------------
class ReNameMain p where
  reName :: p -> Identity p 


instance (CNode a ) => ReNameMain (CTranslationUnit a) where
  reName (CTranslUnit ls a) = do
    ls' <- mapM reName ls
    return (CTranslUnit ls' a) 

instance (CNode a ) => ReNameMain (CExternalDeclaration a) where
  reName  (CFDefExt a) = do
    a'<-reName a
    return (CFDefExt a')
  reName  g = return g

instance (CNode a ) => ReNameMain (CFunctionDef a) where
  reName a@(CFunDef a1 (CDeclr (Just (Ident s a2 a3)) a4 a5 a6 a7) a8 a9 a10) = do
    if ((getIdentName a) == "main") then return (CFunDef a1 (CDeclr (Just (Ident (prefix ++ s) a2 a3)) a4 a5 a6 a7) a8 a9 a10)
    else return a
--

--
addMain preBlock block (CTranslUnit ls a) =(CTranslUnit (ls ++ preBlock ++ [(createMain block)]) a)


createMain block = CFDefExt (CFunDef [CTypeSpec (CIntType undefNode)] (CDeclr (Just (Ident "main" 0 undefNode)) [CFunDeclr (Right ([],False)) [] undefNode] Nothing [] undefNode) [] (CCompound [] block undefNode) undefNode)

-------------------------------
-- Static Declarations 
-------------------------------
mainHead u' i'  = (lu u') ++ (li i') -- ++ [charDecl (infix_++"c"),intDecl (infix_++"i")]
  where
    li 0 = []
    li i = (map (\x-> intDecl_ ("i"++show x)) [1..i])
    lu 0 = []
    lu u = (map (\x-> intDecl_ ("u"++show x)) [1..u]) ++ (map (\x->attrU x) [1..u])
-------------------------------
--  Simple Declarations
-------------------------------
charDecl c = CBlockDecl (CDecl [CTypeSpec (CCharType undefNode)] [(Just (CDeclr (Just (Ident (prefix ++ c) 0 undefNode)) [] Nothing [] undefNode),Nothing,Nothing)] undefNode)

intDecl i = CBlockDecl (CDecl [CTypeSpec (CIntType undefNode)] [(Just (CDeclr (Just (Ident (prefix ++ i) 0 undefNode)) [] Nothing [] undefNode),Nothing,Nothing)] undefNode)

intDecl_ i = CBlockDecl (CDecl [CTypeSpec (CIntType undefNode)] [(Just (CDeclr (Just (Ident (atribVars ++ i) 0 undefNode)) [] Nothing [] undefNode),Nothing,Nothing)] undefNode)
-------------------------------
--  Remaning Declarations 
-------------------------------

declare (a,b,c) =  CBlockDecl $ fmap (const undefNode) (CDecl b [(Just (CDeclr (Just a) c Nothing [] ()),Nothing,Nothing)] ())
-- ^ type declaration tipos simples
-- same as above  only using strings


-- Function Call
callFun :: String -> PreInstrumentedVar -> [CCompoundBlockItem NodeInfo]
callFun name ls = [CBlockStmt (CExpr (Just (CCall (CVar (Ident name 0 undefNode) undefNode) ls' undefNode)) undefNode)]
    where 
      app i = (CVar i undefNode)
      ls' = map (app.(\(a,_,_)->a)) ls
      
      

-----------------------------
--  Vars declaration  of locations
-----------------------------
specLoc i = CDeclExt (CDecl [CTypeSpec (CIntType undefNode)] [(Just (CDeclr (Just (Ident (location ++ (show i)) 0 undefNode)) [] Nothing [] undefNode),Just (CInitExpr (CConst (CIntConst (cInteger 0) undefNode)) undefNode),Nothing)] undefNode)



