----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
-- Declaration of coverage location  variables.
-----------------------------------------------------------------------------
module PreInstrumentation.Variables.LocationVars where

import PreInstrumentation.Statements 
import Language.C.Syntax.AST
import Types.Annotations
import Language.C.Data.Node
import Language.C.Syntax.AST

locationInitVars i ii (CTranslUnit ls a) = (CTranslUnit ( h++ls ) a)
  where 
    h = map (switch . specLoc) [i..ii]

switch = (fmap (\x-> ([],[],[],x)::Annot ))


