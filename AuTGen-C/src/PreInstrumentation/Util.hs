----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
--Um módulo contendo
-----------------------------------------------------------------------------
module PreInstrumentation.Util where

import Language.C.Data.Ident
import Language.C.Data.Node
import Language.C.Syntax.AST

getIdentName (CFunDef _ (CDeclr (Just (Ident s _ _))_ _ _ _) _ _ _) = s
getIdentName _ = ""

getT(CTranslUnit a _) = a

appPrefix s (Ident a b c) = (Ident (s++a) b c)


type PreInstrumentedVar = [( Ident, [CDeclarationSpecifier ()], [CDerivedDeclarator ()] )]

-- | AUX functions - Check functions to observe the type of array
checkIsArray =  and . map chk
  where
    chk (CArrDeclr [] _ ()) = True
    chk _ = False
 

checkArray = and . map chk
  where
    chk (CArrDeclr [] (CNoArrSize False) ()) = False
    chk (CArrDeclr [] (CArrSize False (CVar _ _) ) () ) = False
    chk _ = True

dropLast []=[]
dropLast (x:[]) = []
dropLast (x:xs) = x : dropLast xs

zipConcat:: [a] ->  [[a]] -> [a]
zipConcat [] [] = []
zipConcat [] s = concat s
zipConcat s [] = s
zipConcat (x:xs) (y:ys) = (x:y) ++ (zipConcat xs ys)
