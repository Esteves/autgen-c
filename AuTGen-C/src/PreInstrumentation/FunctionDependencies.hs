{-# LANGUAGE FlexibleInstances #-}
----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
-- It is responsable for obtaining all the functions that derives for a inicial one.
-----------------------------------------------------------------------------
module PreInstrumentation.FunctionDependencies (getFunDependenciesOf,getFunDependencies) where
--
import PreInstrumentation.Util
--
import Language.C.Data.Ident
import Language.C.Syntax.AST
import Control.Monad.State.Lazy
import Language.C.Data.Node
import Data.List
import Data.Maybe

import qualified Data.Map as M
--
import Language.C.Pretty as P


type FunDependencies = (Ident ,M.Map Ident [Ident]) -- Ident is for inside job.

------------
-- Shared --
------------
getFunDependenciesOf s =  (\x-> evalState (funUnionM (getIdentOf s x)) (getFunDependencies x , M.keys $ getFunDependencies x ,   (getIdentOf s x) ) ) 

getFunDependencies :: (CNode a ) => (CTranslationUnit a) -> M.Map Ident [Ident]
getFunDependencies = (\x-> snd $ execState (funDep x ) (Ident "" 1 undefNode ,M.empty))
-------------
-------------
getIdentOf :: String -> CTranslationUnit t -> [Ident]
getIdentOf s ast = map getIdent $ filter (\x->getIdentName x == s) $ map gF $ filter (iF)$ gT ast
  where
   getIdent (CFunDef _ (CDeclr (Just s)_ _ _ _) _ _ _) = s
   gT (CTranslUnit a _) = a 
   iF (CFDefExt _)= True
   iF _ = False  
   gF (CFDefExt a) = a



-- | Who Depend of him  
type Union = ( M.Map Ident [Ident],[Ident] , [Ident])
-- ^ List of relation ,  all funcion declared in file, output
-- snd one is to avoid function declared external to the file.
funUnionM [] = do
  (_,_,out) <-get
  return out
funUnionM ks = do 
    (m,k,out) <- get
    new <- return $ concat $ catMaybes $ map (\x-> M.lookup x m) ks
    upDate new
    funUnionM (new \\ out)


upDate ls = do 
    (a,b,c) <-get
    put (a,b,intersect (union c ls) b)
    




-- | Class
class FunctionDependacie p where
  funDep :: p -> State FunDependencies ()

instance (CNode a ) => FunctionDependacie (CTranslationUnit a) where
  funDep (CTranslUnit ls _) = mapM_ funDep ls

instance (CNode a ) => FunctionDependacie (CExternalDeclaration a) where
  funDep (CDeclExt a) = return ()
  funDep (CFDefExt a) = funDep a
  funDep (CAsmExt b a)  = return ()


instance (CNode a ) => FunctionDependacie (CFunctionDef a) where
-- ^ CFunDef [CDeclarationSpecifier a] (CDeclarator a) [CDeclaration a] (CStatement a) a
  funDep  (CFunDef ls1 a ls2 b c) = if hasIdent a
       then do
          modify  ((<->)  (const ident ) (M.insert  (ident) []))
          funDep b
      else return ()
    where
      ident = getIdent a
      hasIdent (CDeclr (Just a) _ _ _ _) = True
      hasIdent _                         = False
      getIdent (CDeclr (Just a) _ _ _ _) = a
      (<->) f g (a,b) = (f a, g b)

instance (CNode a ) => FunctionDependacie (CStatement a) where
-- | @ CLabel Ident (CStatement a) [CAttribute a] a @ An (attributed) label followed by a statement
  funDep (CLabel _ b _ _) = funDep b
-- | @ CCase (CExpression a) (CStatement a) a @ A statement of the form case expr : stmt
-- CExpression are simple...
  funDep (CCase _ b _) = funDep b
-- | @ CCases (CExpression a) (CExpression a) (CStatement a) a @ A case range of the form case lower ... upper : stmt 
  funDep (CCases _ _ b _) = funDep b
-- | @CDefault (CStatement a) a @ The default case default : stmt
  funDep (CDefault b _ ) = funDep b
-- | @CExpr (Maybe (CExpression a)) a@ A simple statement, that is in C: evaluating an expression with side-effects and discarding the result.
  funDep (CExpr (Just b) _) =funDep b
-- | @ CCompound [Ident] [CCompoundBlockItem a] a @ compound statement CCompound localLabels blockItems at
  funDep (CCompound _ ls _) = mapM_ funDep ls
-- | @ CIf (CExpression a) (CStatement a) (Maybe (CStatement a)) a @ conditional statement CIf ifExpr thenStmt maybeElseStmt at
  funDep (CIf a b (Nothing) _)= funDep a >> funDep b
  funDep (CIf a b (Just c) _) = funDep a >> funDep b >> funDep c
-- | @ CSwitch (CExpression a) (CStatement a) a @ switch statement CSwitch selectorExpr switchStmt, where switchStmt usually includes case, break and default statements
  funDep (CSwitch a b _) = funDep a >> funDep b
-- | @ CWhile (CExpression a) (CStatement a) Bool a @ while or do-while statement CWhile guard stmt isDoWhile at 
  funDep (CWhile a b _ _) = funDep a >> funDep b
-- | @ CFor (Either (Maybe (CExpression a)) (CDeclaration a)) (Maybe (CExpression a)) (Maybe (CExpression a)) (CStatement a) a @ for statement CFor init expr-2 expr-3 stmt, where init is either a declaration or initializing expression  
-- (2^3 + 4 options)
  funDep (CFor (Left (Just a))  (Just b)  (Just c ) d _)  = funDep a >> funDep b >> funDep c >> funDep d
  funDep (CFor (Left (Just a))  Nothing  (Just c ) d _)   = funDep a >> funDep c >> funDep d
  funDep (CFor (Left (Just a))  (Just b)  Nothing d _)    = funDep a >> funDep b >> funDep d
  funDep (CFor (Left (Just a))  Nothing  Nothing d _)     = funDep a >> funDep d

  funDep (CFor (Left Nothing)  (Just b)  (Just c ) d _) = funDep b >> funDep c >> funDep d
  funDep (CFor (Left Nothing)  Nothing  (Just c ) d _ ) = funDep c >> funDep d
  funDep (CFor (Left Nothing)  (Just b)  Nothing d _)   = funDep b >> funDep d
  funDep (CFor (Left Nothing)  Nothing  Nothing d _)    = funDep d

  funDep (CFor _  (Just b)  (Just c) d _) = funDep b >> funDep c >> funDep d
  funDep (CFor _  Nothing  (Just c) d _)  = funDep c >> funDep d 
  funDep (CFor _  (Just b)  Nothing d _)  = funDep b >> funDep d
  funDep (CFor _  Nothing  Nothing d _)   = funDep d

-- | CGotoPtr (CExpression a) a @ computed goto CGotoPtr labelExpr
  funDep (CGotoPtr b _) = funDep b
-- | @ CReturn (Maybe (CExpression a)) a @ return statement CReturn returnExpr
  funDep (CReturn (Just b) _) = funDep b
-- | Others ...  
  funDep _ = return ()
{-- ^
CGoto Ident a @ goto statement CGoto label
CGotoPtr (CExpression a) a @ computed goto CGotoPtr labelExpr
CCont a @ continue statement
CBreak a @ break statement
CAsm (CAssemblyStatement a) a @ assembly statement
Instances-}


instance (CNode a ) => FunctionDependacie (CExpression a) where
-- |@ CComma [CExpression a] a   @
  funDep (CComma ls _) =  mapM_ funDep ls
-- |@ CAssign CAssignOp (CExpression a) (CExpression a) a        @
  funDep (CAssign _ b c _) = funDep b >> funDep c
-- |@ CCond (CExpression a) (Maybe (CExpression a)) (CExpression a) a    @
  funDep (CCond b (Just c) d _)  = funDep b >> funDep c >> funDep d
  funDep (CCond b (Nothing) c _) = funDep b >> funDep c
-- |@ CBinary CBinaryOp (CExpression a) (CExpression a) a        @
  funDep (CBinary _ b c _)  = funDep b >> funDep c
-- |@ CCast (CDeclaration a) (CExpression a) a   @
  funDep (CCast _ b _) = funDep b
-- |@ CUnary CUnaryOp (CExpression a) a)  @
  funDep (CUnary _ b _) = funDep b
-- |@ CSizeofExpr (CExpression a) a      @
  funDep (CSizeofExpr b _) = funDep b
-- |@ CAlignofExpr (CExpression a) a     @
  funDep (CAlignofExpr b _) = funDep b
-- |@ CComplexReal (CExpression a) a     @
  funDep (CComplexReal b _) = funDep b
-- |@ CComplexImag (CExpression a) a     @
  funDep (CComplexImag b _) = funDep b
-- |@ CIndex (CExpression a) (CExpression a) a   @ 
  funDep (CIndex b c _) = funDep b >> funDep c
-- |@ CCall (CExpression a) [CExpression a] a    @             !!TODO!!
  funDep (CCall (CVar a _) lsc _) = modify ( insert a) >> mapM_ funDep lsc
    where
      insert x (y,z) =(y,  M.insertWith (++) y [x]  z)
  funDep (CCall (CMember _ _ _ _) b _) = return ()
  funDep (CCall a b _) = error ("Chamada de função desconhecida... bug at FunctionDependacies.hs\n" ++ ( show $ map (const ()) b) ++ "\n"  ++ (show $fmap (const ()) a) ++ "\n"  )
--TODO
-- |@ CMember (CExpression a) Ident Bool a       @
  funDep (CMember b _ _ _) = funDep b
-- |@ CStatExpr (CStatement a) a        @ GNU C compound statement as expr @
  funDep (CStatExpr b _) = funDep b
-- |@ CSizeofType (CDeclaration a) a     @
-- |@ CAlignofType (CDeclaration a) a    @
-- |@ CVar Ident a       @
-- |@ CConst (CConstant a)      @ integer, character, floating point and string constants @
-- |@ CCompoundLit (CDeclaration a) (CInitializerList a) a@ C99 compound literal @
-- |@ CLabAddrExpr Ident a      @ GNU C address of label @
-- |@ CBuiltinExpr (CBuiltinThing a) @ GNU Builtins, which cannot be typed in C99
  funDep _ = return ()
  
instance (CNode a ) => FunctionDependacie (CCompoundBlockItem a) where
-- |@ CBlockStmt (CStatement a)@ A statement
  funDep (CBlockStmt b) = funDep b
-- |@ CBlockDecl (CDeclaration a) @ A local declaration
-- |@ CNestedFunDef (CFunctionDef a) @ A nested function (GNU C)
  funDep _ = return ()
-- TODO Function dependeci to (GNU C) 


instance (FunctionDependacie a ) => FunctionDependacie (Maybe a) where
  funDep (Just a) = funDep a
  funDep Nothing = return ()
  
--END Class
