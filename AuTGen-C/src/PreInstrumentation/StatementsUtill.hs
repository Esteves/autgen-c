----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
--Um módulo contendo
-----------------------------------------------------------------------------
module PreInstrumentation.StatementsUtill where
--
import Configurations
import PreInstrumentation.Util

--

import Language.C.Syntax.AST
import Language.C.Pretty
import Language.C.Data.Node
import Language.C.Data.Ident
import Control.Monad.Identity
import Language.C.Syntax.Constants




-- | assigment to the unsignedvar
attrU i = CBlockStmt (CExpr (Just (CAssign CAssignOp (CVar (Ident (atribVars++"u"++show i) 0 undefNode) undefNode) (CCall (CVar (Ident "nondet_uint" 0 undefNode ) undefNode) [] undefNode) undefNode)) undefNode)


attrF_Ident name nonDetType bool = CBlockStmt (CExpr (Just (CAssign CAssignOp (CVar name undefNode) (CCall (CVar (Ident nonDet 0 undefNode ) undefNode) [] undefNode) undefNode)) undefNode)
  where
    nonDet = if(bool) then (typeSpecF nonDetType)++"Ptr"
                      else (typeSpecF nonDetType)

attrF_Ident_C name nonDetType bool = CBlockStmt (CExpr (Just (CAssign CAssignOp (CVar name undefNode) (CCall (CVar (Ident nonDet 0 undefNode ) undefNode) [] undefNode) undefNode)) undefNode)
  where
    nonDet = if(bool) then (typeSpecF nonDetType)++"Ptr"++"C"
                      else (typeSpecF nonDetType)++"C"



attrFPtr_Ident name nonDetType =  CBlockStmt (CExpr (Just (CAssign CAssignOp (CUnary CIndOp (CVar name undefNode) undefNode) (CCall (CVar (Ident nonDet 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)
  where
    nonDet = (typeSpecF nonDetType)


{-attrF_String name nonDetType bool = CBlockStmt (CExpr (Just (CAssign CAssignOp (CVar (Ident name 0 undefNode) undefNode) (CCall (CVar (Ident nonDet 0 undefNode ) undefNode) [] undefNode) undefNode)) undefNode)
  where
    nonDet = if(bool) then (typeSpecF nonDetType)++"Ptr"
                      else (typeSpecF nonDetType)
-}

-- | Class Unique Ident  was created to impose easy way  to compare 
--class UniqueIdent a where






















-- OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD OLD 





-----------------------------
--  Utill
-------------------------------
-- | Ident typeSpecV
typeSpecV c (CCharType ()) = (Ident (prefix ++ c) 0 undefNode)
typeSpecV i (CIntType ()) = (Ident (prefix ++ i) 0 undefNode)


--  | typeSpecR random attribution
typeSpecR c (CCharType ()) = setRandChar (c++"c")
typeSpecR i (CIntType ()) = setRandInt (i++"i")
--  | 
--typeSpecF (CCharType ()) = randomChar 
--typeSpecF (CIntType ()) = randomInt 

-- [(Ident "READ_COMMAND_LINE_DEVICES" 369909963 (NodeInfo ("InstrumentedCode.c": line 2) (("InstrumentedCode.c": line 2),25) (Name {nameId = 1})),Nothing),(Ident "READ_DEVICES" 231941882 (NodeInfo ("InstrumentedCode.c": line 2) (("InstrumentedCode.c": line 2),12) (Name {nameId = 2})),Nothing),(Ident "SKIP_DEVICES" 257239547 (NodeInfo ("InstrumentedCode.c": line 2) (("InstrumentedCode.c": line 2),12) (Name {nameId = 3})),Nothing)]

--typeSpecF b = error (show b)
typeSpecF (CEnumType (CEnum Nothing (Just b) [] _) _) = random ++ (concat $ map (getIdent.fst) b)
  where
    getIdent (Ident a _ _) = a

typeSpecF b = random ++ (  filter (\x-> x /= ' ') .  show . pretty $ fmap (const undefNode) b)


------------------------------
--        Assiments         --
------------------------------

-- setRand<> declaration and attribution


setRandInt i = CBlockDecl (CDecl [CTypeSpec (CIntType undefNode)] [(Just (CDeclr (Just (Ident (prefix ++ i) 0 undefNode)) [] Nothing [] undefNode),Just (CInitExpr (CCall (CVar (Ident randomInt 0 undefNode) undefNode) [] undefNode) undefNode),Nothing)] undefNode)

setRandChar c = CBlockDecl (CDecl [CTypeSpec (CCharType undefNode)] [(Just (CDeclr (Just (Ident (prefix ++ c) 0 undefNode)) [] Nothing [] undefNode),Just (CInitExpr (CCall (CVar (Ident randomChar 0 undefNode) undefNode) [] undefNode) undefNode),Nothing)] undefNode)


