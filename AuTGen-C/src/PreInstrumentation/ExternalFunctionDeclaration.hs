{-|
Module      : ExternalFunctionDeclaration
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental
Details     : 
  
-}
module PreInstrumentation.ExternalFunctionDeclaration(
    externalFunctionDecl
  , getIdentType
  , cleanStaticAtrr
  , cleanStaticAtrrT
)where

import Language.C.Data.Node (undefNode)
import Language.C.Data.Ident
import Language.C.Syntax.AST
import Language.C.Pretty

import Types.EqUtil

import Configurations

externalFunctionDecl lsIdent lsType = (CDeclExt  (CDecl ([CStorageSpec (CExtern ())]++lsIdent )  [(Just (CDeclr (Just (Ident name 0 undefNode ))  ([CFunDeclr (Right ([],False)) [] ()] ++ lsType) Nothing [] ()),Nothing,Nothing)] ()))
  where
    name = if (lsType == [CPtrDeclr [] ()] ) then ( (funcNonDet (getIdentType lsIdent)) ++ "Ptr")
           else funcNonDet (getIdentType lsIdent)

-- | Obter identidade do tipo
getIdentType :: [CDeclarationSpecifier t] -> String
getIdentType [] = ""
getIdentType (n@(CTypeSpec (_)):ls) = filter (\x-> x /= ' ') $ show $ pretty  $ fmap (const undefNode) n
getIdentType (_:ls) = getIdentType ls


-- | Remove Static attribute declaration
cleanStaticAtrr :: [CDeclarationSpecifier ()] -> [CDeclarationSpecifier ()]
cleanStaticAtrr ((CStorageSpec (CStatic ())):ls) = ls
cleanStaticAtrr l = l 

cleanStaticAtrrT (a,b,c) = (a, cleanStaticAtrr b, c)
