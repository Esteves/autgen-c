----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
-- This is reponsable for assigned the varibles so it module
--   possible user inputs.
--
--
-----------------------------------------------------------------------------
module PreInstrumentation.StatementsDeclarations where
--
import Configurations
import PreInstrumentation.Util
--
import Language.C.Syntax.AST
import Language.C.Data.Node
import Language.C.Data.Ident
import Control.Monad.Identity
import Language.C.Syntax.Constants

import Control.Monad.State
import Control.Applicative 

import PreInstrumentation.StatementsUtill


type Struct = Int 

cleanNoSizeAttr :: [ (Ident, [CDeclarationSpecifier ()], [CDerivedDeclarator ()]) ] 
          ->  ([ (Ident, [CDeclarationSpecifier ()], [CDerivedDeclarator ()]) ], Int)

cleanNoSizeAttr ls = evalState ( {-setSpecialDef-} return ls >>= mapM cleanNoSizeAttr_ >>= setReturn) 0
-- ^ hierarchy   (char or int )

setReturn :: a -> State Struct (a,Int)
setReturn a = do
  u <- get
  return (a,u)

-- Insert cases that run out.
-- size 1 array intDecl

-- | é para fazer f (Int a ,char [a]) as expeciais...
setSpecialDef ls | length ls /=2 = return ls
                 | otherwise = do
                      ls' <- return . i3 $ last_ ls
                      if   (checkIsArray  ls')
                      then do
                            i <- return . getI . i1 $ head ls
                            l <-return  $ last_ ls'
                            if (i== getI' l ) then do
                                 ls'' <- return $ dropLast ls' ++ [setNew l]
                                 return $ dropLast ls ++ [  f3 ls'' (last_ ls)]
                            else return ls
                      else return ls
  where
    i1 = (\(x,_,_) -> x)
    i3 = (\(_,_,x) -> x)
    f3 f = (\(a,b,_) -> (a,b,f))
    chkCvar (CArrDeclr [] (CArrSize False (CVar _ _) ) () ) = True
    chkCvar _ = False
    getI  (Ident s _ _) = s
    getI' (CArrDeclr [] (CArrSize False (CVar s _) ) () ) = getI s
    setNew (CArrDeclr [] (CArrSize False (CVar (Ident s s' l) s'') ) () ) = 
        (CArrDeclr [] (CArrSize False (CVar (Ident ( prefix ++ s) s' l) s'') ) () )
    last_ [] = error "ups"
    last_ x = last x

-- others..
cleanNoSizeAttr_ (a,b,c) = do
    c' <- mapM cleanNoSizeAttr__ c
    return (a,b,c')
    

cleanNoSizeAttr__ (CArrDeclr [] (CNoArrSize False) ()) = do
    modify (+1)
    u <- get
    return  (CArrDeclr [] (CArrSize False  (CVar (Ident (atribVars++"u"++(show u)) 0 undefNode) ()) ) () )

cleanNoSizeAttr__ a  =  return a

--
