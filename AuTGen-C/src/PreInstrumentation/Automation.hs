-----------------------------------------------------------------------
{-|
Module      : Automation
Description : 
Copyright   : (c) Fábio Esteves Sousa, 2014
License     : 
Maintainer  : 
Stability   : experimental
-}
------------------------------------------------------------------------
module PreInstrumentation.Automation where
--
import PreInstrumentation.FunctionVars (getFunVars)
import PreInstrumentation.GlobalVars
import PreInstrumentation.Statements
import PreInstrumentation.Util (zipConcat,appPrefix)
import PreInstrumentation.ExternalFunctionDeclaration
import Configurations
--
import Language.C hiding (pretty, parseC)
import Control.Monad.Identity
import Control.Monad.State.Lazy

import Data.List


preInstrumentation  :: FilePath -> String -> CTranslationUnit NodeInfo -> CTranslationUnit NodeInfo
preInstrumentation fileName funName = (\x -> evalState (preInst fileName funName x) () )

preInst :: FilePath -> String -> (CTranslationUnit NodeInfo) -> State () (CTranslationUnit NodeInfo)
preInst fileName  funcName ast = do 
  -- obtaining Vars type
  gVars <- (return . removeConstVars. getGlobalVars fileName) ast
  fVars <- return $ getFunVars funcName ast
  -- renaming  Var names 
  (prefixFVars,u) <- return $ cleanNoSizeAttr fVars
  prefixFVars' <- return $ app1 prefixFVars --fVars
  -- Creating declarations 
  declFvars <- return $ map declare prefixFVars'

  -- Set Attribution  
  (assgGVarsAux,vAux,s) <- return $ toAssign gVars
  (assgGVars,v) <- return $ appPair concat id (assgGVarsAux,vAux)
  (assgFVars,v',s') <- return $ toAssign prefixFVars'
  -- remove main
  newAst <- return $ runIdentity (reName ast)
  -- Add main 
  preBlock <- return $ map ( fmap (const undefNode) . uncurry externalFunctionDecl ) (union s s')
  return $ addMain preBlock (mainHead u (max v' v) ++ assgGVars  ++ ( zipConcat declFvars assgFVars) ++  (callFun funcName prefixFVars') ) newAst
--  error $ show $ fmap (const () ) gVars
--  error $ show $ (preBlock!!0)

    where 
      app1 = map  (appF (appPrefix  prefix )  )
      appF f (a,b,c) = (f a,b,c)
      mapF = (\x-> concat . (map x))
      appPair f g (a,b) = (f a, g b)
