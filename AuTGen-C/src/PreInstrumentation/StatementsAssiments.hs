----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
-- This is reponsable for assigned the varibles so it module
--   possible user inputs.
--
--
-----------------------------------------------------------------------------
module PreInstrumentation.StatementsAssiments where
--
import Configurations
import PreInstrumentation.Util
--
import Language.C.Syntax.AST
import Language.C.Data.Node
import Language.C.Data.Ident
import Control.Monad.Identity
import Language.C.Syntax.Constants

import Types.EqUtil

import Control.Monad.State
import Control.Applicative 

import PreInstrumentation.StatementsUtill
import PreInstrumentation.ExternalFunctionDeclaration 

import Data.List

type ExternalF = [ ( [CDeclarationSpecifier ()], [CDerivedDeclarator ()] ) ]
type Struct = (Int,Int, Int, [ (Int,CExpression NodeInfo) ], ExternalF) -- ^ Max value, int of varU, Iterator, List, list  struct types need to create external functions

--(CVar (Ident ("t"++show t) 0 undefNode) undefNode)

maxOf :: a -> State Struct a
maxOf v = do
  (x,u,y,_,s) <- get
  if (x>y) then modify (const (x,u,0,[],s))
  else modify (const (y,u,0,[],s))
  return v

setU :: State Struct Int
setU = do
  (x,u,y,z,s) <-get
  let var = (CVar (Ident (atribVars ++"u"++( show (u+1)) ) 0 undefNode) undefNode)
  modify (const (x,(u+1),y+1,(y+1,var):z,s) )
  return (y+1)
    
--getI :: CExpression NodeInfo -> State Struct Int
getI v = do
  (x,u,y,z,s) <- get
  modify (const (x,u,y+1,(y+1,v):z,s))
  return (y+1)

setAddExternalF a b = do
  (x,u,y,z,s) <-get
  modify (const (x,u,y,z, union s [(a,b)] ))
---

toAssign
  :: [(Ident, [CDeclarationSpecifier ()], [CDerivedDeclarator ()])]
     -> ([[CCompoundBlockItem NodeInfo]], Int, ExternalF)
toAssign ls = evalState (mapM (toAssign_ . cleanStaticAtrrT) ls >>= setReturn) (0,0,0,[],[])
-- ^ hierarchy   (char or int )
--   const char *s;
--   char x;
--   char *const t;
--   char *const t;
--   char array [1]
--       simple only with numbers or vars or one [].

setReturn :: [[a]] -> State Struct ([[a]],Int,ExternalF)
setReturn a = do
  (v,u,_,_,s) <- get
  return (a,v,s) 

-- # NOTE # With change to cbmc 5 from now one all variables which need random attributed, it will be assign values from external
-- functions. At point the cbmc is no possible to retrieve from trace correct values.

{-TODO delete this comment 
s = &c;
--                prefix_infix_c = nondet...
--                s= &prefix_infix_c;
-}


-- TODO
--(Ident "directories_args" 361733009 (NodeInfo ("grep.c": line 373) (("grep.c": line 373),16) (Name {nameId = 25200})),[CTypeSpec (CCharType ()),CTypeQual (CConstQual ())],[CArrDeclr [] (CNoArrSize False) (),CPtrDeclr [CConstQual ()] ()])




-- | Remove struct declarations.

{-TODO remove it
[CTypeSpec (CEnumType _)]
-}


--toAssign_ ( a,  [CTypeSpec (CSUType (CStruct CStructTag (Just (Ident "stat" 244873843 (NodeInfo ("cfile.c": line 8) (("cfile.c": line 8),4) (Name {nameId = 11})))) Nothing [] ()) ()),CTypeQual (CConstQual ())]

--toAssign_( a, b, c) = error (show a ++ " >.< "++show b ++ " >.< "++show c )

toAssign_ (a,[CTypeSpec b@(CEnumType _ _)],[]) = do
    return [attrF_Ident a b False]
    --error (show "1")

toAssign_ (_, (CStorageSpec _):ls, _) = return []

toAssign_ (_,[CTypeSpec (_),CTypeQual (CConstQual ())], [CArrDeclr [] (_) (),CPtrDeclr [CConstQual ()] ()] ) = return []

toAssign_ (_,[CTypeSpec (_),CTypeQual (CConstQual ())], [CPtrDeclr [CConstQual ()] ()] ) = return []

-- | const <char> *s; 
toAssign_ (a, [CTypeQual (CConstQual ()),CTypeSpec (b)], [CPtrDeclr [] ()]) = do
    setAddExternalF [CTypeSpec (b)] [CPtrDeclr [] ()]
    return [attrF_Ident a b True]
    --error (show "2")

--   static char const *lastout;
--   int const *lim
toAssign_ (a, [CTypeSpec (b),CTypeQual (CConstQual ())], [CPtrDeclr [] ()]) = do
    setAddExternalF [CTypeSpec (b)] []
    return [attrF_Ident a b True]
    --error (show "3")

-- | const <char> s;
toAssign_ (a, [CTypeQual (CConstQual ()),CTypeSpec b], []) = return [] -- Erro?? pois não é possivel atribuir valores deste tipo???????????????????

-- | char x; x=nondet...
toAssign_ (a,[CTypeSpec (b)],[]) = do
    setAddExternalF [CTypeSpec (b)] []
    return [attrF_Ident a b False]
    --error (show "4")



toAssign_ (a,[CTypeSpec (b)],[CPtrDeclr [] ()]) = do
    setAddExternalF [CTypeSpec (b)] [CPtrDeclr [] ()]
    return [attrF_Ident a b True]
    --error (show "5")



-- | char *const t; 
toAssign_ (a,[CTypeSpec (b)],[CPtrDeclr [CConstQual ()] ()]) = do
    setAddExternalF [CTypeSpec (b)] []
    return [attrFPtr_Ident a b]  --       attrFPtr ==> *t=NONDET_CHAR();
    --error (show "6")

-- | @CFunDeclr@  to Ignore 
toAssign_ (_,_,[CFunDeclr _ _ _] ) = return []



------------------
-- | Array  | --
------------------
toAssign_ all@(a, b'@[CTypeSpec (b)] , ls)
  | (checkIsArray ls) {-&& (checkArray ls)-}  = do
      setAddExternalF [CTypeSpec (b)] []
      ls' <- arrayCombination ls 
      ls'' <- return $  map (declrToDeclrAssigment b . combinationToDecl a) ls' 
      setVarsAndPointerArrayInit ls'' >>= maxOf
 --     s<-get 
 --     error ("what???"++(show s) ++"<.>")
  | otherwise =
      error ("\nin (StatementsAssiments.hs ) (1)\nNot implemented yet...\n" ++ (show all) )

toAssign_ all =  return [] --error (" in (StatementsAssiments.hs ) (2)\nNot implemented yet...\n" ++ (show all) )
-- static char const *lastout


declrToDeclrAssigment b a = CBlockStmt (CExpr (Just (CAssign CAssignOp a (CCall (CVar (Ident (typeSpecF b) 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)

arrayCombination ls = do
     ls' <- mapM (set ) ls
     return $ combination ls'

combination = foldr (\x y -> concat $ map (\x'-> map (\y'-> x':y')  y)    x )  [[]] 

--      return .  map (\x-> setArrayB b x) . map (\y -> combinationToDecl a y)  $ arrayCombination ls

-----------------------------------------------------------------------------------
-- |  function that decided what to do with each element of the array to be treat 
-----------------------------------------------------------------------------------
set :: CDerivedDeclarator () -> State Struct [CExpression NodeInfo]
set (CArrDeclr [] (CArrSize False (CConst (CIntConst i ()))) ()) = mapM  app  [0..((getCInteger i)-1)]
  where
    app = (\x->  return $ (CConst (CIntConst (cInteger x) undefNode)) )

-- !!!!!!!!!!!!!!!!!!!fmap (const undefNode) em a

set (CArrDeclr [] (CArrSize False a ) ()) = getI a' >>= (\x -> return [(CVar (Ident (atribVars++"i"++(show x) ) 0 undefNode ) undefNode)])
  where 
    a' = fmap (const undefNode) a
    
set (CArrDeclr [] (CNoArrSize False) () ) = setU   >>= (\x -> return [(CVar (Ident (atribVars++"u"++(show x) ) 0 undefNode ) undefNode)])
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
combinationToDecl a ls = foldl (\x y-> CIndex x y undefNode) (CVar a undefNode)  ls


-- | function responsable for set the cicles that inicialize de variavel's or undefined size in the arrays
setVarsAndPointerArrayInit:: [CCompoundBlockItem NodeInfo] -> State Struct [CCompoundBlockItem NodeInfo]
setVarsAndPointerArrayInit node = do
    (_,_,_,ls,_) <- get
    return $ foldr (\(i,max) node' ->  forStatment (toVar i ) max node' ) node ls
  where
    toVar x = (CVar (Ident (atribVars++"i"++(show x) ) 0 undefNode ) undefNode)

--(Int, Int, [ (Int,CExpression NodeInfo) ] ) 

forStatment var max node = [CBlockStmt (CFor (Left (Just (CAssign CAssignOp var (CConst (CIntConst (cInteger 0) undefNode)) undefNode))) (Just (CBinary CLeOp var max undefNode)) (Just (CUnary CPostIncOp var undefNode)) (CCompound [] node undefNode) undefNode)]




 
--(CArrSize False (CConst (CIntConst 2 ())))

-- [CArrDeclr [] (CArrSize False (CConst (CIntConst (cInteger 2) ()))) (),CArrDeclr [] (CArrSize False (CConst (CIntConst (cInteger 2) ()))) ()]

--array  = map fromApp
--  where 
--    fromApp  (CArrSize False (CConst n ))   =   Right $ getCInteger n
  --  toApp Right n = (CConst (CIntConst (cInteger n) undefNode))

--[CBlockStmt (CExpr (Just (CAssign CAssignOp (CIndex (CIndex (CVar a undefNode) (CConst (CIntConst (cInteger n1) undefNode)) undefNode) (CConst (CIntConst (cInteger n2) undefNode)) undefNode) (CCall (CVar (Ident (typeSpecF b) 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)]

-- Saved to future implementations
{- return [ CBlockStmt (CFor (Left (Just (CAssign CAssignOp (CVar (Ident "i" 105 undefNode) undefNode) (CConst (CIntConst (cInteger 0) undefNode)) undefNode))) (Just (CBinary CLeOp (CVar (Ident "i" 105 undefNode) undefNode) (CVar (Ident "t" 116 undefNode ) undefNode) undefNode)) (Just (CUnary CPostIncOp (CVar (Ident "i" 105 undefNode) undefNode) undefNode)) (CCompound [] [CBlockStmt (CExpr (Just (CAssign CAssignOp (CIndex (CVar (Ident "c" 99 undefNode) undefNode) (CVar (Ident "i" 105 undefNode ) undefNode) undefNode) (CCall (CVar (Ident "nondet_char" 422601531 undefNode) undefNode) [] undefNode) undefNode)) undefNode)] undefNode) undefNode)]


CBlockStmt (CExpr (Just (CAssign CAssignOp (CVar (Ident "t" 116 (NodeInfo ("../sandbox/tests/array2.c": line 4) (("../sandbox/tests/array2.c": line 4),1) (Name {nameId = 8}))) undefNode) (CCall (CVar (Ident "nondet_uint" 460384700 (NodeInfo ("../sandbox/tests/array2.c": line 4) (("../sandbox/tests/array2.c": line 4),11) (Name {nameId = 10}))) undefNode) [] undefNode) undefNode)) undefNode),

-}

-- | char a [];
--[CArrDeclr [] (CNoArrSize False) ()]
-- | char a [n];
{-- toAssign_ (a, [CTypeSpec (b)] , [CArrDeclr [] (CArrSize False (CConst (CIntConst n ())) ) ()]) = return . concat $ map (\x-> toAux a b x) [0..((getCInteger n)-1)]
  where 
    toAux a b n = [CBlockStmt (CExpr (Just (CAssign CAssignOp (CIndex (CVar a undefNode) (CVar (Ident (show n) 0 undefNode ) undefNode) undefNode) (CCall (CVar (Ident (typeSpecF b) 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)]

-- | char a [n1][n2]
toAssign_ (a, [CTypeSpec (b)] , [CArrDeclr [] (CArrSize False (CConst (CIntConst n1 ()))) (),CArrDeclr [] (CArrSize False (CConst (CIntConst n2 ()))) ()] ) =  return .concat $ map (\x -> toAux a b x) ls
  where
    xl = [0..((getCInteger n1)-1)]
    yl = [0..((getCInteger n2)-1)]
    ls = concat $ appTo2 xl yl
    toAux a b (n1,n2) = [CBlockStmt (CExpr (Just (CAssign CAssignOp (CIndex (CIndex (CVar a undefNode) (CConst (CIntConst (cInteger n1) undefNode)) undefNode) (CConst (CIntConst (cInteger n2) undefNode)) undefNode) (CCall (CVar (Ident (typeSpecF b) 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)]

-- | char a [n];
toAssign_ (a, [CTypeSpec (b)] , [CArrDeclr [] (CArrSize False n ) ()]) = do
    return [CBlockStmt (CFor (Left (Just (CAssign CAssignOp (CVar (Ident (atribVars++"i") 0 undefNode) undefNode) (CConst (CIntConst (cInteger 0) undefNode)) undefNode))) (Just (CBinary CLeOp (CVar (Ident (atribVars++"i") 0 undefNode) undefNode) n' undefNode)) (Just (CUnary CPostIncOp (CVar (Ident (atribVars++"i") 0 undefNode) undefNode) undefNode)) (CCompound [] [CBlockStmt (CExpr (Just (CAssign CAssignOp (CIndex (CVar a undefNode) (CVar (Ident (atribVars++"i") 0 undefNode ) undefNode) undefNode) (CCall (CVar (Ident (typeSpecF b) 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)] undefNode) undefNode)]
    where n' = fmap (const undefNode) n
--}
-- | char a [n]
-- |char aa [2][22]



-- | ENDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD


{-  TODO remove comment and possible other upthere.


-- | char *const t; *t=NONDET_CHAR();
--                  *t=nondet...
toAssign_ (a,[CTypeSpec (CTypeOfType _ _)],[CPtrDeclr [CConstQual ()] ()]) = error "BANG Bang bang..."
toAssign_ (a,[CTypeSpec (b)],[CPtrDeclr [CConstQual ()] ()]) = return [CBlockStmt (CExpr (Just (CAssign CAssignOp (CUnary CIndOp (CVar a undefNode) undefNode) (CCall (CVar (Ident (typeSpecF b) 0 undefNode) undefNode) [] undefNode) undefNode)) undefNode)]


-}
