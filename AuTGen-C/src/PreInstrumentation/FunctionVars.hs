{-# LANGUAGE FlexibleInstances #-}
----------------------------------------------------------------------------
-- |
-- Module       : 
-- Description  :
-- Copyright    : Fábio Sousa 
--
--
-- It is responsable for obtaining the input variables of a function.
-----------------------------------------------------------------------------
module PreInstrumentation.FunctionVars (
  getFunVars
) where
--
import Language.C.Syntax.AST
import Control.Monad.State.Lazy
import Language.C.Data.Node
--
import PreInstrumentation.Util
import PreInstrumentation.GlobalVars



getFunVars s = (\x-> declarToPreInstVar  . snd $ execState (funVar x) (s,[]))

type FunVar = (String, [CDeclaration ()])
class FunctionVars p where
  funVar :: p -> State FunVar ()

instance CNode a => FunctionVars (CTranslationUnit a) where
  funVar (CTranslUnit ls _) = mapM_ funVar ls

instance CNode a => FunctionVars (CExternalDeclaration a) where
  funVar (CAsmExt _ _) = return ()
  funVar (CDeclExt _) = return ()
  funVar (CFDefExt a) = do
    (s,ls) <-get 
    if ((isF a) && ((getIdentName a) == s)) then funVar a-- put (s, ls ++ [a])
     else return ()
   where
      toApp = isF 
      isF (CFunDef ls1 a ls2 b c) = hasIdent a
      hasIdent (CDeclr (Just a) _ _ _ _) = True
      hasIdent _                         = False

instance CNode a => FunctionVars (CFunctionDef a) where
  funVar (CFunDef _ a b _ _) = do
      funVar a
      (s,ls) <-get 
      put (s, ls ++(clean b))

clean :: [(CDeclaration a)] -> [(CDeclaration ())] 
clean = map (fmap (\x-> () ))
  
instance CNode a => FunctionVars (CDeclarator a) where
  funVar (CDeclr _ a b _ _) = mapM_ funVar a


instance CNode a => FunctionVars ( CDerivedDeclarator a) where
  funVar (CFunDeclr (Left _) _ _) = return ()
  funVar (CFunDeclr (Right (a, _) ) _ _) = do
      (s,ls) <-get 
      put (s, ls ++ (clean a))
  funVar _ = return ()
