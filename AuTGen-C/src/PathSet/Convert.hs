{-# LANGUAGE FlexibleInstances #-}
module PathSet.Convert (gen) where

import Types.CGF

import Types.Annotations
import Control.Monad.State
import Language.C.Syntax.AST
import Language.C.Data.Node
import Language.C.Syntax.Constants (cInteger, getCInteger) 
import Language.C.Data.Position (nopos)
import Language.C.Data.Ident

--Test
--import Automation
--import  Instrumentation.Automation (switchToAnnot)
--import Teste
--

import Control.Applicative
import qualified  Data.Map as M


type St = M.Map String [CGF Int String]

-- aux Function --
getIdentMaybe (CDeclr (Just (Ident s _ _))_ _ _ _) = Just s
getIdentMaybe _ = Nothing

allVar (a,b,c,_) = toIds $ fromVars a ++ fromVars b ++ fromVars c 
dualVar (a,b,c,_) = ( toIds $ fromVars b ,  toIds $ fromVars c)

fromVars = map fromVar
fromVar (Var i) = i 
toIds = map toId
toId i = Id i





-------------------------------------------------------------------
gen :: CTranslationUnit Annot -> M.Map String [CGF Int String]
gen l = execState (convert l) M.empty


class ConvertToCGF p where
    -- |
    convert :: p -> State St [CGF Int String]


instance ConvertToCGF (CTranslationUnit Annot) where
    convert (CTranslUnit a b) = do
        mapM_ convert a >>= return . const []
    -- ^ CTranslUnit [CExternalDeclaration a] a

instance ConvertToCGF (CExternalDeclaration Annot) where
    convert (CFDefExt a) = convert a
    convert _ = return []
    -- ^ CDeclExt (CDeclaration a)
    -- ^ CFDefExt (CFunctionDef a)
    -- ^ CAsmExt (CStringLiteral a) a

instance ConvertToCGF (CFunctionDef Annot) where
    convert (CFunDef _ b _ d e) = case (getIdentMaybe b) of
      Nothing -> return []
      Just n -> do
        d' <- convert d
        modify (M.insert n (allVar e ++ d'))
        return []
   -- ^ CFunDef [CDeclarationSpecifier a] (CDeclarator a) [CDeclaration a]  (CStament a) a

instance ConvertToCGF (CStatement Annot) where
    convert (CIf a b Nothing d ) = do
      a' <- convert a
      b' <- convert b
      (t,f) <- return $ dualVar d
      return [If ((a' ++ t ++ b'), (a'++ f))]
      
    convert (CIf a b (Just c) d ) = do  
      a' <- convert a
      b' <- convert b
      c' <- convert c
      (t,f) <- return $ dualVar d
      return [If ((a' ++ t ++ b'), (a'++ f ++c'))]
   -- ^ CIf (CExpression a) (CStatement a) (Maybe (CStatement a)) a


    convert (CCase a b c) = do 
      b' <- convert b
      return $ [Case ((allVar c)++b')]--(allVar c)++b' -- TODO Break and no break paths
   -- ^ CCase (CExpression a) (CStatement a) a

    convert (CCases a b c d) = do
      c' <- convert c
      return $ [Case ((allVar d)++c')]     -- TODO Break and no break paths
    -- ^ CCases (CExpression a) (CExpression a) (CStatement a) a {- gcc implements an extension to the C language case 1...4: is valid -}

    convert (CDefault a b ) = do
      a' <- convert a
      return $ [Case ((allVar b)++a')] -- TODO Break and no break paths
    -- ^ CDefault (CStatement a) a






    convert (CLabel a b c d) = concat <$> mapM convert c
   -- ^ CLabel Ident (CStatement a) [CAttribute a] a

    convert (CCompound a b c ) = concat <$> mapM convert b 
   -- ^ CCompound [Ident] [CCompoundBlockItem a] a

    convert (CWhile a b c d) = do
      a' <- convert a
      b' <- convert b
      (t,f) <- return $ dualVar d
      return [Cycle ((a' ++ f ), (a'++ t ++b'))]
   -- ^ CWhile (CExpression a) (CStatement a) Bool a

    convert (CFor a b c d e) = do
          a' <- either (maybe (return []) convert ) (convert) a
          b' <- maybe (return []) convert b
          c' <- maybe (return []) convert c
          d' <- convert d
          (t,f) <- return $ dualVar e
          return (a'   ++ [Cycle ((b' ++ f ), (b'++ t ++d'++c'))])
   -- ^ CFor (Either (Maybe (CExpression a)) (CDeclaration a)) (Maybe (CExpression a)) (Maybe (CExpression a)) (CStatement a) a


    convert (CSwitch a b c) = do
          a' <- convert a 
          b' <- convert b
          return  (a' ++ [Switch b'] ) -- $ a'++ [Case  b']
 -- ^ CSwitch (CExpression a) (CStatement a) a 

    convert (CExpr b a) = maybe (return []) convert b
   -- ^ CExpr (Maybe (CExpression a)) a

    convert (CGotoPtr b a) = convert b
   -- ^ CGotoPtr (CExpression a) a

    convert (CReturn b a) = do 
      b'<- maybe (return []) convert b
      return (b'++ [R])
   -- ^ CReturn (Maybe (CExpression a)) a

    convert (CBreak a) = do 
      return ([B])
   -- ^ CBreak a
    convert (CCont a) = do 
      return ([Cont])
   -- ^ CCont a
   
   -- TODO (iff so)
   -- ^ CGoto Ident a 
    convert _ = return []

   -- ^ CAsm (CAssemblyStatement a) a

instance ConvertToCGF (CCompoundBlockItem Annot) where
    convert (CBlockStmt a) = convert a
   -- ^ CBlockStmt (CConvertToCGF a) 
    convert (CBlockDecl a)  = convert a 
   -- ^ CBlockDecl (CDeclaration a)
    convert (CNestedFunDef a)  = convert a
   -- ^ CNestedFunDef (CFunctionDef a) 

instance ConvertToCGF (CDeclarator Annot) where
    convert (CDeclr _ _ _ a _) = concat <$> mapM convert a
      -- ^ CDeclr (Maybe Ident) [CDerivedDeclarator a] (Maybe (CStringLiteral a)) [CAttribute a] a
      -- Possible spacial cases that declare function not as usual.

instance ConvertToCGF (CDeclaration Annot) where
    convert  (CDecl _ b c) = concat <$> mapM func b
      where 
        func = liftT3 f1 f2 f3 
        f1 = maybe (return [] )  (convert)
        f2 = maybe (return [] )  (convert)
        f3 = maybe (return [] )  (convert)  
        liftT3 f g h (a,b,c) = do
            a' <- f a
            b' <- g b
            c' <- h c
            return $ a'++ b'++c'
  -- ^ CDecl [CDeclarationSpecifier a] [(Maybe (CDeclarator a), Maybe (CInitializer a), Maybe (CExpression a))] a

instance ConvertToCGF (CArraySize Annot) where
    convert (CArrSize _ a) = convert a
    convert _ = return []
   -- ^ CNoArrSize Bool 
   -- ^ CArrSize Bool (CExpression a) 

instance ConvertToCGF (CExpression Annot) where
    convert (CCall (CVar (Ident n _ _) _) e _) = do
      e' <- concat <$> mapM convert e
      return (e'++[C n])
   -- ^ CCall (CExpression a) 
    convert (CStatExpr a b) = convert a
   -- ^ CStatExpr (CConvertToCGF a) a 
    convert (CComma a _) = concat <$> mapM convert a
   -- ^ CComma [CExpression a] a
    convert (CCond a b c d) = do
        a' <- convert a
        b' <- maybe (return []) convert b
        c' <- convert c
        return [If (a'++b',a'++c')]
   -- ^ CCond (CExpression a) (Maybe (CExpression a)) (CExpression a) a
    convert (CSizeofExpr a _)  = convert a
   -- ^ CSizeofExpr (CExpression a)
    convert (CSizeofType a _) = convert a
   -- ^ CSizeofType (CDeclaration a) 
    convert (CAlignofExpr a _) = convert a
   -- ^ CAlignofExpr (CExpression a)
    convert (CAlignofType a _) = convert a
   -- ^ CAlignofType (CDeclaration a) 
    convert (CComplexReal a _) = convert a
   -- ^ CComplexReal (CExpression a) 
    convert (CComplexImag a _) = convert a
   -- ^ CComplexImag (CExpression a) 
    convert (CIndex _ a _) = convert a
   -- ^ CIndex (CExpression a) 
    convert (CMember a _ _ _) = convert a
   -- ^ CMember (CExpression a) 
    convert (CBuiltinExpr a) = convert a
   -- ^ CBuiltinExpr (CBuiltinThing a) 
    convert (CCompoundLit _ a _) = convert a
   -- ^ CCompoundLit (CDeclaration a)
    convert (CCast _ a _) = convert a
   -- ^ CCast (CDeclaration a) 

    convert _ = return []
   -- ^ CAssign CAssignOp 
   -- ^ CBinary CBinaryOp 
   -- ^ CUnary CUnaryOp 
   -- ^ CVar Ident 
   -- ^ CConst (CConstant a) 
   -- ^ CLabAddrExpr Ident a 

---------------------------------------------------------------------

instance ConvertToCGF (CAttribute Annot) where
    convert (CAttr _ a _) = concat <$> mapM convert a
 -- ^CAttr Ident [CExpression a] a


instance ConvertToCGF (CInitializer Annot) where
    convert (CInitExpr a b) = convert a
   -- ^ CInitExpr (CExpression a) a
    convert (CInitList a b) = convert a
   -- ^ CInitList (CInitializerList a) a

   
instance ConvertToCGF (CInitializerList Annot) where
    convert l = concat <$> mapM func l
      where 
        f1 = convert
        func (a,b) = do
          a' <-  concat <$> mapM f1 a
          b' <- convert b
          return $ b'++ a'
 -- ^ [ ([CPartDesignator a], CInitializer a) ] 



instance ConvertToCGF (CBuiltinThing Annot) where
    convert (CBuiltinVaArg a b _) = do
       a' <- convert a
       b' <- convert b
       return $ a' ++ b'
    -- ^ (CBuiltinVaArg (CExpression a) (CDeclaration a) a)
    convert (CBuiltinOffsetOf a b _) = do
        a' <- convert a
        b' <- concat <$> mapM convert b
        return $ b' ++ a'
    -- ^ (CBuiltinOffsetOf (CDeclaration a) [CPartDesignator a] a)
    convert (CBuiltinTypesCompatible a b _) = do
        a' <- convert a
        b' <- convert b
        return $ b' ++ a'
    -- ^ (CBuiltinTypesCompatible (CDeclaration a) (CDeclaration a) a) 

instance ConvertToCGF (CPartDesignator Annot) where
    convert (CArrDesig a _) = convert a
   -- ^ CArrDesig (CExpression a) a
    convert (CRangeDesig a b _) = do
        a' <- convert a
        b' <- convert b
        return $ a' ++ b'
   -- ^ CRangeDesig (CExpression a) (CExpression a) a
    convert _ = return []
   -- ^ CMemberDesig Ident a

{-

















-}
{-


 -- ---------------------------------------- --  
 -- ---------------------------------------- --
 




******************************************************
instance ConvertToCGF (CDerivedDeclarator Annot) where
    convert a = return a
   -- ^ CFunDeclr (Either [Ident] ([CDeclaration a],Bool)) [CAttribute a] a
   -- ^ CPtrDeclr [CTypeQualifier a] a
   -- ^ CArrDeclr [CTypeQualifier a] (CArraySize a) a




instance ConvertToCGF (CCompoundBlockItem Annot) where
    convert (CBlockStmt a) = do
        a' <- convert a
        return $ CBlockStmt a'      
   -- ^ CBlockStmt (CConvertToCGF a) 
    convert (CBlockDecl a) = do 
        a' <- convert a
        return $  CBlockDecl a'      
   -- ^ CBlockDecl (CDeclaration a)
    convert (CNestedFunDef a) = do 
        a' <- convert a
        return $ CNestedFunDef a'          
   -- ^ CNestedFunDef (CFunctionDef a) 

instance ConvertToCGF (CDeclarationSpecifier Annot) where
   convert (CStorageSpec a) = do
          a' <- convert a
          return $ CStorageSpec a'      
   -- ^ CStorageSpec (CStorageSpecifier a) 
   convert (CTypeSpec a) = do
          a' <- convert a
          return $ CTypeSpec a'      
   -- ^ CTypeSpec (CTypeSpecifier a) 
   convert (CTypeQual a) = do
          a' <- convert a
          return $ CTypeQual a'      
   -- ^ CTypeQual (CTypeQualifier a) 

instance ConvertToCGF (CStorageSpecifier Annot) where
    convert a = return a
   -- ^ CAuto a 
   -- ^ CRegister a 
   -- ^ CStatic a 
   -- ^ CExtern a 
   -- ^ CTypedef a 
   -- ^ CThread a 

instance ConvertToCGF (CTypeSpecifier Annot) where
    convert a = return a
   -- ^ CVoidType a
   -- ^ CCharType a
   -- ^ CShortType a
   -- ^ CIntType a
   -- ^ CLongType a
   -- ^ CFloatType a
   -- ^ CDoubleType a
   -- ^ CSignedType a
   -- ^ CUnsigType a
   -- ^ CBoolType a
   -- ^ CComplexType a
   -- ^ CSUType (CStructureUnion a) a 
   -- ^ CEnumType (CEnumeration a) a 
   -- ^ CTypeDef Ident a 
   -- ^ CTypeOfExpr (CExpression a) a 
   -- ^ CTypeOfType (CDeclaration a) a 

instance ConvertToCGF (CTypeQualifier Annot) where
    convert a = return a
   -- ^ CConstQual a
   -- ^ CVolatQual a
   -- ^ CRestrQual a
   -- ^ CInlineQual a
   -- ^ CAttrQual (CAttribute a)

instance ConvertToCGF (CStructureUnion Annot) where
    convert a = return a
   -- ^ CStruct
   
instance ConvertToCGF(CStructTag) where
    convert a = return a
  -- ^ CUnionTag
  -- ^ CStructTag

instance ConvertToCGF (CEnumeration Annot) where
    convert a = return a
   -- ^ CEnum (Maybe Ident) (Maybe [(Ident, Maybe (CExpression a))]) [CAttribute a]
            



   
instance ConvertToCGF (CAttribute Annot) where
    convert a = return a


instance ConvertToCGF (CExpression Annot) where
    convert (CStatExpr a b) = do
          a' <- convert a
          return $ CStatExpr a' b
   -- ^ CStatExpr (CConvertToCGF a) a 
    convert a = return a
   -- ^ CComma [CExpression a] 
   -- ^ CAssign CAssignOp 
   -- ^ CCond (CExpression a) 
   -- ^ CBinary CBinaryOp 
   -- ^ CCast (CDeclaration a) 
   -- ^ CUnary CUnaryOp 
   -- ^ CSizeofExpr (CExpression a)
   -- ^ CSizeofType (CDeclaration a) 
   -- ^ CAlignofExpr (CExpression a)
   -- ^ CAlignofType (CDeclaration a) 
   -- ^ CComplexReal (CExpression a) 
   -- ^ CComplexImag (CExpression a) 
   -- ^ CIndex (CExpression a) 
   -- ^ CCall (CExpression a) 
   -- ^ CMember (CExpression a) 
   -- ^ CVar Ident 
   -- ^ CConst (CConstant a) 
   -- ^ CCompoundLit (CDeclaration a)
   -- ^ CLabAddrExpr Ident a 
   -- ^ CBuiltinExpr (CBuiltinThing a) 

instance ConvertToCGF (CBuiltinThing Annot) where
    convert a = return a
   -- ^ CBuiltinVaArg (CExpression a) (CDeclaration a) a 
   -- ^ CBuiltinOffsetOf (CDeclaration a) [CPartDesignator a] a 
   -- ^ CBuiltinTypesCompatible (CDeclaration a) (CDeclaration a) a 

instance ConvertToCGF (CConstant Annot) where
    convert a = return a
   -- ^ CIntConst CInteger a
   -- ^ CCharConst CChar a
   -- ^ CFloatConst CFloat a
   -- ^ CStrConst CString a


instance ConvertToCGF (CStringLiteral Annot) where
  convert a = return a
    -- ^ CStrLit CString a

 
-----------------------------------------------------------------------------
--------------------- UTIL --------------------------------------------------

                          
                          
                         
                          
empty = ([],[],[], OnlyPos nopos (nopos,-1) )
                          
zero = cConst  0
cConst i = (CConst (CIntConst (cInteger i) empty ) )                          
assert i = Var i
pair f (a,b) = (f a , b)
-}
