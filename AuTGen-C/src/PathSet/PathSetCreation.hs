-- | Improve possivel by use Set Data.

module PathSet.PathSetCreation where

import Control.Monad.State.Lazy
import Data.List (head, group , sort , concat , tail , map , permutations ,subsequences, foldr )
import Data.Map as M hiding (filter,map)
import Control.Applicative (liftA2)

import Types.CGF

data Label a  = Return a 
              | Continue a
              | Break a
              | Normal a deriving (Show,Eq,Ord)

instance Functor Label where
  fmap f (Continue x) = (Continue (f x))
  fmap f (Normal x) = (Normal (f x))
  fmap f (Return x) = (Return (f x))
  fmap f (Break x)  = (Break (f x))

value (Normal x) = x
value (Return x) = x
value (Break x) = x
value (Continue x) = x



-- TODO  change the deep control ----  f(g(f(g()))) is 4 even call only 2 times
-- TODO TODO make the return xD

--  ## TESTING ## --
test n v = alg n v empty
testV1 = [(Cycle ([Id 0],[If ([Id 1,R],[Id 2]), If ([Id 3],[Id 4])]))]
testV2 = [(Cycle ([Id 0],[If ([Id 1,R],[Id 2]), If ([Id 3],[B])]))]
m1 = M.fromList [("Acompile",[]),("Ecompile",[]),("GAcompile",[]),("Gcompile",[]),("PAcompile",[]),("add_count",[]),("all_zeros",[]),("buffer_textbin",[]),("cc_gen_main",[]),("clean_up_stdout",[]),("color_cap_mt_fct",[]),("color_cap_ne_fct",[]),("color_cap_rv_fct",[]),("contains_encoding_error",[]),("context_length_arg",[]),("dcnpgettext_expr",[]),("dcpgettext_expr",[]),("dos_binary",[]),("dos_unix_byte_offsets",[]),("dossified_pos",[]),("fgrep_to_grep_pattern",[]),("file_textbin",[]),("fillbuf",[]),("get_nondigit_option",[]),("grep",[Id 1,If ([Id 2,R],[Id 3]),If ([Id 4,C "suppressible_error",R],[Id 5]),If ([Id 6],[Id 13,If ([C "textbin_is_binary",Id 7,If ([Id 8,R],[Id 9])],[C "textbin_is_binary",Id 12,If ([Id 10],[Id 11])])]),Cycle ([Id 51],[Id 14,If ([Id 15],[Id 16]),If ([Id 17,B],[Id 18]),C "zap_nuls",If ([Id 19],[Id 20]),If ([Id 21,If ([Id 22],[Id 23]),If ([Id 24,C "prpending"],[Id 25]),If ([Id 26],[Id 27])],[Id 28]),Cycle ([Id 32],[Id 29,Cycle ([Id 31],[Id 30])]),If ([Id 33,If ([Id 34],[Id 35]),If ([Id 36],[Id 37])],[Id 38]),If ([Id 39],[Id 40]),If ([Id 41,C "nlscan"],[Id 42]),If ([Id 43,C "suppressible_error"],[Id 44]),If ([Id 45,C "buffer_textbin",If ([C "textbin_is_binary",Id 46,If ([Id 47,R],[Id 48])],[C "textbin_is_binary",Id 49])],[Id 50])]),If ([Id 52,If ([Id 53],[Id 54]),If ([Id 55,C "prpending"],[Id 56])],[Id 57]),If ([Id 58,C "gettext",C "printf"],[Id 59]),R]),("grep_command_line_arg",[]),("grepbuf",[]),("grepdesc",[]),("grepdirent",[]),("grepfile",[]),("guess_type",[]),("init_easy_encoding",[]),("is_device_mode",[]),("main",[]),("mb_clen",[]),("nlscan",[]),("npgettext_aux",[]),("open_symlink_nofollow_error",[]),("parse_grep_colors",[]),("pgettext_aux",[]),("pr_sgr_end",[]),("pr_sgr_end_if",[]),("pr_sgr_start",[]),("pr_sgr_start_if",[]),("prepend_args",[]),("prepend_default_options",[]),("print_filename",[]),("print_line_head",[]),("print_line_middle",[]),("print_line_tail",[]),("print_offset",[]),("print_sep",[]),("prline",[]),("prpending",[]),("prtext",[]),("reset",[]),("setmatcher",[]),("skip_easy_bytes",[]),("skipped_file",[]),("suppressible_error",[]),("textbin_is_binary",[]),("to_uchar",[]),("undossify_input",[]),("usable_st_size",[]),("usage",[]),("x2nrealloc",[]),("xcharalloc",[]),("xnmalloc",[]),("xnrealloc",[]),("zap_nuls",[])]

p1 = [Id 14,If ([Id 15],[Id 16]),If ([Id 17,B],[Id 18]),C "zap_nuls",If ([Id 19],[Id 20]),If ([Id 21,If ([Id 22],[Id 23]),If ([Id 24,C "prpending"],[Id 25]),If ([Id 26],[Id 27])],[Id 28]),Cycle ([Id 32],[Id 29,Cycle ([Id 31],[Id 30])]),If ([Id 33,If ([Id 34],[Id 35]),If ([Id 36],[Id 37])],[Id 38]),If ([Id 39],[Id 40]),If ([Id 41,C "nlscan"],[Id 42]),If ([Id 43,C "suppressible_error"],[Id 44]),If ([Id 45,C "buffer_textbin",If ([C "textbin_is_binary",Id 46,If ([Id 47,R],[Id 48])],[C "textbin_is_binary",Id 49])],[Id 50])]

p2 =  [Cycle ([Id 51],[Id 14,If ([Id 15],[Id 16]),If ([Id 17,B],[Id 18]),C "zap_nuls",If ([Id 19],[Id 20]),If ([Id 21,If ([Id 22],[Id 23]),If ([Id 24,C "prpending"],[Id 25]),If ([Id 26],[Id 27])],[Id 28]),Cycle ([Id 32],[Id 29,Cycle ([Id 31],[Id 30])]),If ([Id 33,If ([Id 34],[Id 35]),If ([Id 36],[Id 37])],[Id 38]),If ([Id 39],[Id 40]),If ([Id 41,C "nlscan"],[Id 42]),If ([Id 43,C "suppressible_error"],[Id 44]),If ([Id 45,C "buffer_textbin",If ([C "textbin_is_binary",Id 46,If ([Id 47,R],[Id 48])],[C "textbin_is_binary",Id 49])],[Id 50])])]


data St a b = St { deep::Int , maxDeep::Int, list:: Map b  [CGF a b]} deriving Show

-- |aux
plus :: State (St a b) ()
plus = do
  s<- get
  modify (const  (s { deep = 1 + deep s}) )

less :: State (St a b) ()
less = do
  s<- get
  modify (const  (s { deep =  (deep s)-1 }) )

check :: State (St a b) Bool
check = do
  s<- get
  return $ (deep s) < (maxDeep s)  -- < or <=... < as will be +1

alg  k ls list = map value $ evalState (  {-liftM rmdups (liftM (map rmdups)-}  (algSt (k) ls)) {-)-} (St 0 (k) list )

algAUX  k ls list = evalState (  {-liftM rmdups (liftM (map rmdups)-}  (algSt (k) ls)) {-)-} (St 0 (k) list )


--algSt :: Ord a => (Int -> Bool) -> [CGF a] -> State St [[a]]

--algSt _ a = error $ show a

algSt f [] = return [Normal[]]


algSt f (R:ls) = return [Return []]
algSt f (Cont:ls) = return [Continue  []]
algSt f (B:ls) = return [Break  []]


algSt f ((Id a):ls) = do
    algA <- algSt f ls
    return (unionCGF ([ (Normal [a])])  algA)  
    
algSt f ((If (a,b)):ls) = do
    algA <-   algSt f a 
    algB <-  algSt f b
    alg' <-  algSt f ls 
    return $ (unionCGF algA alg') ++ (unionCGF algB alg')


algSt f ((C a):ls) = do
    v <- get
    return $ seq  v ()
    --error $ show v
    alg' <- algSt f ls
    b <- check
    if (b) then do
        plus
        s <- get
        cgf <- return . maybe [] id  $ M.lookup a (list s)
        algA' <- algSt f cgf 
        algA <- return $ map cleanCall algA'
        less
        --v <- get
        --error $ show algA
        return $ (unionCGF algA alg')
    else return alg'


algSt k ((Cycle (b,a)):ls) = do -- F T
    --ss <- get
    --error (show ss) 
    algA  <- (algSt k a >>= return . paths k) 
--    error (show algA)
    algB <- algSt k  b
    alg'  <- algSt k ls
    alg'' <- return $ algA ++ [Normal []]
    --error $ ( show alg') ++ (show b)
    return $ (unionCGF alg'' (unionCGF algB alg') )
    --err <- algSt k a
    --error $ ( show algA)

-- | 

algSt k ((Switch l):ls) = do --undefined 
    alg' <-  algSt k ls
    algLst <- mapM (algSt k . fromCase) l
--    error $ show algLst
    algA <- return $ map ( unifyPathSwt . concat) $ tails algLst
--    error $ show algA
    return $ unionCGF algA alg'

algSt _ a = error $ show a
fromCase (Case a)= a


{-
algx f [] = [[]]
algx f ((Id a):ls) = (unionCGFx [[a]] $ algx f ls)  
algx f ((If (a,b)):ls) = (unionCGFx algA alg') ++ (unionCGFx algB alg')
  where 
    algA =  algx f a 
    algB =  algx f b
    alg' =  algx f ls 

algx k ((Cycle (a,b)):ls) =  (unionCGFx (unionCGFx (algA) algB) alg')
  where 
    algA = fullpaths k $ algx k a 
    algB = algx k b
    alg' = algx k ls  
-}

--unionCGF :: [[a]] ->sp [[a]] -> [[a]]
p = [['a'],['b'],['z']]

paths f = rmdups .  map (fmap rmdups ) . map ( cleanBreak.cleanCont.unifyPath ). concat  . map permutations . comb f
--pathsInSwitch = tails





-- | The break value need to get out after size paths have been establish. body cycles need to be clean to "return" or "normal"
cleanBreak (Break a) = Normal a
cleanBreak a = a

cleanCont (Continue a) = Normal a
cleanCont a = a

cleanCall (Return a) = Normal a
cleanCall (Break a) = error "Break type after function call ??-??"
cleanCall (Continue a) = error "Break type after function call ??-??"
cleanCall a = a


unifyPath :: [Label  [a]] -> Label [a]
unifyPath [] = Normal []
unifyPath ((Return a):_) = (Return a)
unifyPath ((Break a):_) = (Break a)
unifyPath ((Normal a):ls) =  fmap (a++) (unifyPath ls)
unifyPath ((Continue a):ls) =  fmap (a++) (unifyPath ls)

unifyPathSwt :: [Label  [a]] -> Label [a]
unifyPathSwt [] = Normal []
unifyPathSwt ((Normal a):ls) =  fmap (a++) (unifyPathSwt ls)
unifyPathSwt ((Return a):_) = (Return a)
unifyPathSwt ((Continue a):_) = (Continue a)
unifyPathSwt ((Break a):_) = (Normal a)

--fullpaths f ls = concat .  map (map concat ). filter ((f).length.head). map permutations . tail $ subsequences ls 

rmdups :: (Ord a) => [a] -> [a]
rmdups = map head . group . sort

single a = [a]

unionCGF :: Ord a => [Label [a]] ->
 [Label [a]] -> [Label [a]] 
unionCGF l ll = rmdups  $ map (fmap rmdups ) $ liftA2 (unifyCGF) l ll--undefined -- rmdups .  map (feither rmdups ) $ liftA2 (unifyCGF) l ll 


unifyCGF (Return a) _ = Return a
unifyCGF (Break a) _ = Break a
unifyCGF (Continue a) _ = Continue a
unifyCGF a b =   fmap (rmdups .  ((value a)++)) b    -- Normal ((value a) ++ (value b))


--unionCGFx l ll =  liftA2 (++) l ll


tails :: [a] -> [[a]]
tails [] = []
tails l = [l] ++ tails (tail l)

comb :: Int -> [a] -> [[a]]
comb m xs = concat $ map (combsBySize xs !!) [1..m]
 where
   combsBySize = Data.List.foldr f ([[]] : repeat [])
   f x next = zipWith (++) (map (map (x:)) ([]:next)) next

